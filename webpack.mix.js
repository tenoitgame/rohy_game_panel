let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.copy('resources/assets/js/chartjs.min.js', 'public/js')
	.js('resources/assets/js/dashboard.js', 'public/js')
	.copy('resources/assets/js/less.min.js', 'public/js')
	.js('resources/assets/js/main.js', 'public/js')
	.js('resources/assets/js/plugins.js', 'public/js')
	.copy('resources/assets/js/popper.min.js', 'public/js')
	.js('resources/assets/js/selectFx.js', 'public/js')
	.js('resources/assets/js/widgets.js', 'public/js')
	.copy('resources/assets/js/pagination.js', 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css')
	.sass('resources/assets/sass/socials.scss', 'public/css')
	.sass('resources/assets/sass/style.scss', 'public/css')
	.copy('resources/assets/css/cs-skin-elastic.css', 'public/css')
	.copy('resources/assets/css/flag-icon.min.css', 'public/css')
	.copy('resources/assets/css/font-awesome.min.css', 'public/css')
	.copy('resources/assets/css/normalize.css', 'public/css')
	.copy('resources/assets/css/themify-icons.css', 'public/css')
	.copyDirectory('resources/assets/css/lib', 'public/css/lib')
	.copyDirectory('resources/assets/img', 'public/img')
	.copyDirectory('resources/assets/js/lib', 'public/js/lib')
	.copyDirectory('resources/assets/js/vendor', 'public/js/vendor')
	.copyDirectory('resources/assets/fonts', 'public/fonts')
	.autoload({
        jquery: ['$', 'window.jQuery', 'jQuery'],
    });;
