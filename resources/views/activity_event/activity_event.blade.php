@extends('template.main') 
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
    table { table-layout: fixed; }
    table th, table td { overflow: hidden; padding: 10px;}
</style>
@section('content')
<div class="card">
    @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="card-header">
        <strong class="card-title">Exchange Event</strong>
        <div class="row">
            <a href="#addGameModal" class="btn btn-success rounded" data-toggle="modal">
                <span>Add Exchange Event</span>
            </a>       
        </div>
    </div>
    <div class="card-body">

        <table id="activityEvent" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>GameName</th>
                    <th>Description</th>
                    <th>CreateTime</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var table = $('#activityEvent').DataTable( {
        lengthChange: false,
        ajax: "{{ '/api/activityEvent/get' }}",
        columns: [
            { data: "id", width: "5%"},
            { data: "type", width: "10%" },
            { data: "game_name", width: "10%" },
            { data: "description", width: "15%" },
            { data: "create_time", width: "10%" },
            { data: null, defaultContent:"<a href='#editGameModal' class='edit btn btn-primary rounded' data-toggle='modal'><span>Edit</span></a>","width": "10%"},
        ],
        select: true
    });

    $('#activityEvent tbody').on( 'click', 'a.edit', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        $("#id").val(data['id']);
        $("#Type").val(data['type']);
        $("#GameName").val(data['game_name']);
        $("#Description").val(data['description']);
    });
});
</script>
@endsection

@section('modal')
<div id="addGameModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/activityEvent/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Add Exchange Event</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" name="Type">
                            <option value='INVITATION'>INVITATION</option>
                            <option value="NEWBIE">NEWBIE</option>
                            <option value="SHARE">SHARE</option>
                            <option value="PREREGISTRATION">PREREGISTRATION</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>GameName</label>
                        <select class="form-control" name="GameName">
                            <option value='trsm'>trsm</option>
                            <option value="ch">ch</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" name="Description" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('modal2')
<div id="editGameModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/activityEvent/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Exchange Event</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input id="id" name="id" required style="display: none;">
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" id="Type" name="Type" readonly="readonly">
                            <option value='INVITATION'>INVITATION</option>
                            <option value="NEWBIE">NEWBIE</option>
                            <option value="SHARE">SHARE</option>
                            <option value="PREREGISTRATION">PREREGISTRATION</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>GameName</label>
                        <select class="form-control" id="GameName" name="GameName" readonly="readonly">
                            <option value='trsm'>trsm</option>
                            <option value="ch">ch</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" id="Description" name="Description" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection