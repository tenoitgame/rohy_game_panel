@extends('template.main') 
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
    table { table-layout: fixed; }
    table th, table td { overflow: hidden; padding: 10px;}
</style>
@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Announcement</strong>
        <div class="row">
            <a href="#addModal" class="btn btn-success rounded" data-toggle="modal">
                <span>Add New Announcement</span>
            </a>       
        </div>
    </div>
    <div class="card-body">

        <table class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th>Start Time</th>
                    <th>Active</th>
                    <th>Create Time</th>
                    <th>Action</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var table = $('.table').DataTable( {
        lengthChange: false,
        ajax: "{{ '/api/announcement/get' }}",
        columns: [
            { data: "id", width: "5%"},
            { data: "type", width: "5%" },
            { data: "inner_description", width: "10%" },
            { data: "start_time", width: "10%" },
            { data: "active", width: "5%" },
            { data: "create_time", width: "10%" },
            { data: null, defaultContent:"<a href='#editModal' class='edit btn btn-primary rounded' data-toggle='modal'><span>Edit</span></a>","width": "5%"},
            { data: null, defaultContent:"<a href='#detailModal' class='detail btn btn-primary rounded' data-toggle='modal'><span>Detail</span></a>","width": "5%"},
        ],
        select: true
    });

    $('.table tbody').on( 'click', 'a.edit', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        $("#id").val(data['id']);
        $("#type").val(data['type']);
        $("#description").val(data['inner_description']);
        $("#startTime").val(data['start_time']);
        $("#active").val(data['active']);
    });

    $('.table tbody').on( 'click', 'a.detail', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        $("#id").val(data['id']);

        $.post( "{{url('api/announcement/detail/get')}}", {
            'announcementId': data['id'],
            "_token": $('#token').val()
            },
            function(announcementDetails) {
                $.each(announcementDetails, function(index, object) {
                    if(index == 'id') {
                        index = 'indonesia';
                    }
                    $("#" + 'id_' + index).val(object.id);
                    $("#" + 'title_' + index).val(object.title);
                    tinymce.get('content_' + index).setContent(object.content != null ? object.content : '');
                });
            }
        );
    });
});
</script>
@endsection

@section('modal')
<div id="addModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/announcement/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Add Announcement</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" name="type">
                            <option selected value="SYSTEM">SYSTEM</option>
                            <option value="EVENT">EVENT</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" name="description" required>
                    </div>
                    <div class="form-group">
                        <label>Start Time</label>
                        <input type="text" class="form-control" name="startTime" required>
                    </div>
                    <div class="form-group">
                        <label>Active</label>
                        <select class="form-control" name="active">
                            <option selected value="Y">Y</option>
                            <option value="N">N</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("#addModal [name='startTime']").datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
    });
});
</script>
@endsection

@section('modal2')
<div id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/announcement/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Announcement</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input id="id" name="id" required style="display: none;">
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" name="type" id="type">
                            <option selected value="SYSTEM">SYSTEM</option>
                            <option value="EVENT">EVENT</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" name="description" id="description" required>
                    </div>
                    <div class="form-group">
                        <label>Start Time</label>
                        <input type="text" class="form-control" name="startTime" id="startTime" required>
                    </div>
                    <div class="form-group">
                        <label>Active</label>
                        <select class="form-control" name="active" id="active">
                            <option selected value="Y">Y</option>
                            <option value="N">N</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("#editModal [name='startTime']").datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
    });
});
</script>
@endsection

@section('modal3')
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ydk5luynxg8bhoji64owouw19noqqnsin0t4l6rlyfjtdqei"></script>
<script>
tinymce.init({
    selector:"#detailModal .editor",
    height : "150",
    plugins: "textcolor preview",
    toolbar: "sizeselect | bold italic | fontselect |  fontsizeselect | forecolor | backcolor | preview",
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
});
</script>
<div id="detailModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/announcement/detail/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Announcement Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <ul class="nav nav-tabs">
                    @foreach($languages as $key => $language)
                    <li class="nav-item"><a href="" data-target="#{{ $language }}" data-toggle="tab" class="nav-link small text-uppercase @if($key==0) {{ 'active' }} @endif">{{ $language }}</a></li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($languages as $key => $language)
                    <div id="{{ $language }}" class="tab-pane fade @if($key==0) {{ 'active show' }} @endif">
                        <div class="modal-body">
                            <input id="id_{{ $language }}" name="id[]" style="display: none">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" id="title_{{ $language }}" name="title[]">
                            </div>
                            <div class="form-group">
                                <label>Content</label>
                                <input type="text" class="form-control editor" id="content_{{ $language }}" name="content[]">
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("#editModal [name='startTime']").datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
    });
});
</script>
@endsection