@extends('template.main') 

@section('addCSS')
	<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
    <style type="text/css">
        .break{
            word-break: break-all;
            }
    </style>
@endsection
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ydk5luynxg8bhoji64owouw19noqqnsin0t4l6rlyfjtdqei"></script>
<script>tinymce.init({
            selector:'#eventText,#exchangeText',
            height : "250",
            plugins: "textcolor preview code",
            toolbar: "sizeselect | bold italic | fontselect |  fontsizeselect | forecolor | backcolor | preview",
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
        });</script>
@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">FAQ Main</strong>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <form action="{{url('/api/event/eventlang')}}"  method="post" id="evForm" style="padding: 20px;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="evid" name="evid" value="<?php echo $key;?>">
      <div class="form-group">
        <label for="exampleInputEmail1">Event (in English)</label>
        <input type="text" class="form-control" id="eventname" value="<?php echo $event[0]->name;?>" name="eventname" aria-describedby="emailHelp" readonly>
        <small id="emailHelp" class="form-text text-muted">This is event's name in English.</small>
      </div>
      <div class="form-group">
        <label for="">Select language</label>
            <select id="langid" name="langid" class="form-control">
                <option value="">none</option>
                <option value="en">english</option>
                <option value="id">indonesian</option>
                <option value="vi">vietnamese</option>
                <option value="zh-Hans">chinese</option>
                <option value="th">thai</option>
            </select>
      </div>
      
      <div class="form-group">
        <label for="exampleInputEmail1">Event's name (in selected language)</label>
        <input type="text" class="form-control" id="eventlang" name="eventlang" aria-describedby="emailHelp" placeholder="Type in the question in selected language" required>
        <small id="emailHelp" class="form-text text-muted">Make sure in selected language.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Banner image path (in selected language)</label>
        <input type="text" class="form-control" id="bannerimglang" name="bannerimglang" aria-describedby="emailHelp" placeholder="Type in the banner image path in selected language" required>
        <small id="emailHelp" class="form-text text-muted">Make sure banner path in selected language.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Menu image path (in selected language)</label>
        <input type="text" class="form-control" id="menuimglang" name="menuimglang" aria-describedby="emailHelp" placeholder="Type in the menu image path in selected language" required>
        <small id="emailHelp" class="form-text text-muted">Make sure menu image path in selected language.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Event Description (in selected language)</label>
        <input type="text" class="form-control" id="eventText" name="eventText" placeholder="Type in text in selected language">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Exchange Description (in selected language)</label>
        <input type="text" class="form-control" id="exchangeText" name="exchangeText" placeholder="Type in text in selected language">
      </div>
      <div class="form-group">
        <label for="">Status</label>
            <select id="status" name="status" class="form-control">
                <option  value="Y">Active</option>
                <option  value="N">Non Active</option>
            </select>
      </div>
      <div class="form-group ">
            <button type="submit" class="btn btn-primary rounded col-md-12">Submit</button>
      </div>
    </form>
    <div class="col-md-12">
        <table class="table table-hover table-bordered table-striped datatable" style="width:100%">
            <thead>
                <tr>
                    <th>ID Event</th>
                    <th>Event</th>
                    <th>Lang</th>
                    <th>Event Image</th>
                    <th>Reward Image</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<!-- <script src="/js/jqueryloader.js"></script> -->
<script type="text/javascript">
$('#evForm').submit(function(ev) {
    if(!tinymce.get('eventText').getContent()) {
        alert('please fill up Event Description');
        return false;
    }
    if(!tinymce.get('exchangeText').getContent()) {
        alert('please fill up Exchange Description');
        return false;
    }
    this.submit();
});

@if(session()->has('message'))
    // alert(message);
@endif

    $(document).ready(function() {
        $('.datatable').DataTable({
            processing: false,
            serverSide: false,
            ajax: {
                url : "{{url('api/event/getByIDandLang/')}}",
                type : "POST",
                data: function (d) {
                    d._token = '{{ csrf_token() }}',
                    d.langid =  $('#langid').val(),
                    d.evid = $('#evid').val()
                },
            },
            columns: [
                {data: 'id_ch_events', name: 'id_ch_events'},
                {data: 'name', name: 'name'},
                {data: 'language', name: 'language'},
                {data: 'banner_img', name: 'banner_img',class:"break"},
                {data: 'menu_img', name: 'menu_img'},
            ]
        });
    });
    $( "#langid" ).change(function() {
        
        $.post( "{{url('api/event/getByIDandLangs')}}",
            {
                langid: $('#langid').val(),
                evid: $('#evid').val(),
                _token: '{{ csrf_token() }}',
            },
            function(data) {
                // alert (data[0]);  
                if(data.length>0){
                    $('#eventlang').val(data[0]['name']);
                    $('#bannerimglang').val(data[0]['banner_img']);
                    $('#menuimglang').val(data[0]['menu_img']);
                    tinymce.get('eventText').setContent(data[0]['event_text']);
                    tinymce.get('exchangeText').setContent(data[0]['exchange_text']);
                    $('#status').val(data[0]['status']);
                }else{
                    $('#eventlang').val('');
                    $('#bannerimglang').val('');
                    $('#menuimglang').val('');
                    tinymce.get('eventText').setContent('');
                    tinymce.get('exchangeText').setContent('');
                    $('#status').val('Y');
                }
            }
        );
    });

</script>
@endsection