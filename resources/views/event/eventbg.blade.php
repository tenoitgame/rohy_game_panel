<!DOCTYPE html>
<html onclick="goLink()">
    <head>
        <meta charset="utf-8" />
        <title>Event</title>
        
        <!-- The stylesheet -->
        <style type="text/css">
        	
			html,
			body,
			.container {
			  height: 100%;
			  width: 100%;
			}
			.container {
			  display: flex;
			  align-items: center;
			  justify-content: center;
			  
			}
			.mydiv {
			  background-color: blue;
			  max-width: 950px;
			  margin: 0 auto;
			  /*background:url('{{$event[0]->banner_img}}') no-repeat center center;*/
			}
			.list{
				width:100%;
			}
        </style>
        
        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    
    <body>
    	<div class="container">
		  <div class="mydiv">
		  	<div class="list">
				@foreach ($event->all() as $obj)
					<!-- {{$obj->name}} -->
		  			<img onclick="goLink()" src="{{$obj->banner_img}}"><br>
		  	 	@endforeach
			</div> 
		</div>
        <footer>
	       
        </footer>
        
    </body>
    <script>
	// On mouse-over, execute myFunction
	function goLink() {
	    window.location.href = '/en/event/{{$obj->id}}';
	}
	</script>
</html>