@extends('template.main') 
@section('addCSS')
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
    .dataTables_filter,#DataTables_Table_0_filter {
    display: none!important; 
    }
</style>
@endsection
@section('content')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<div class="card">
    <div class="card-header">
        <strong class="card-title">Event Main</strong>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="col-md-12">
        <table id="tableevent" class="table table-hover table-bordered table-striped datatable" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>View</th>
                    <th>Change</th>
                </tr>
                <tr id="filterrow">
                    <th>Name</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>View</th>
                    <th>Change</th>
                </tr>
            </thead>
        </table>
    </div>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    
    
</div>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

@if(session()->has('message'))
    // alert(message);
@endif
    $(function() {
        loadDT();
        
    });
    function loadPicker(){
        $('input[name="chevent"]').daterangepicker({
            timePicker: true,
            startDate: $('#event_start').val(),
            endDate: $('#event_end').val(),
            locale: {
              format: 'M/DD hh:mm A'
            }
          });
    }
    function loadDT(){
        $("#tableevent").dataTable().fnDestroy()
        $('#tableevent').DataTable({
        processing: false,
        serverSide: false,
        lengthChange: false,
        rowId: 'id',
        ajax: {
            url : "/api/event/getAll",
            type : "POST",
        },
        columns: [
            {data: 'name', name: 'name'},
            {data: 'start', name: 'start'},
            {data: 'end', name: 'end'},
            {data: null,defaultContent:"<button class='view'>View</button>"},
            {data: null,defaultContent:"<button class='change'>Change</button>"},
        ]
        });
        var table = $('#tableevent').DataTable();

        $('#tableevent tbody').on( 'click', 'button.view', function () {
            var row_id = $(this).parents('tr');
            var data = table.row(row_id).data();
            // alert(data['id']);
            window.location = "/event/detail/"+data['id'];
          } );
        $('#tableevent tbody').on( 'click', 'button.change', function () {
            var row_id = $(this).parents('tr');
            var data = table.row(row_id).data();
            // alert(data['id']);
            $('#event_id').val(data['id']);
            $('#event_start').val(data['start']);
            $('#event_end').val(data['end']);
            loadPicker();
            $('#recordModal').modal('show');
            // window.location = "/event/detail/"+data['id'];
          } );
        $('#tableevent thead tr#filterrow th').each( function () {
                var title = $('.datatable thead th').eq( $(this).index() ).text();
                $(this).html( '<input type="text" onclick="stopPropagation(event);" placeholder="Search '+title+'" />' );
            } );
            var table = $('#tableevent').DataTable();

            
           
            $("#tableevent thead input").on( 'keyup change', function () {
                table
                    .column( $(this).parent().index()+':visible' )
                    .search( this.value )
                    .draw();
            } );

          
    }
    function stopPropagation(evt) {
                if (evt.stopPropagation !== undefined) {
                    evt.stopPropagation();
                } else {
                    evt.cancelBubble = true;
                }
            }

</script>
@endsection
@section('modal')
    <style>
        .CLIENT {color:#333;font-size:12px;max-width:70%;border-radius: 3px;border: 1px solid #000;margin-bottom: 10px;}
        .CS {color:#333;font-size:12px;margin-left:30%;max-width:70%;border-radius: 3px;border: 1px solid #CCC;background:#CCC;margin-bottom: 10px;}
        .msg div:nth-child(1) {margin: 5px 15px;overflow:hidden; white-space: nowrap}
        .msg div:nth-child(2) {margin:0px 15px;overflow:hidden;text-overflow:ellipsis; white-space: nowrap; font-size: 10px;color:#666}
        .msg div:nth-child(3) {margin: 5px 15px;text-align:right}
        .msg_text {height:100px;}
        .msg_text textarea {padding:5px;height:80px;width:100%;box-sizing:border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;border-radius: 5px;}
    </style>
    <div class="modal fade" id="recordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><b>Change Period</b></h5>
            <button type="button" class="close recordDone" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <div id="hidden_data" style="display:none">
                <input id="event_id" value="">
                <input id="event_start" value="">
                <input id="event_end" value="">
            </div>
          <div class="modal-body">
            <table style="width:100%; border-collapse: collapse;border: none;">
                <tbody id="recordContent">
                    <input type="text" class="form-control" name="chevent" id="chevent">
                </tbody>
                <tfoot>
                    
                </tfoot>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary recordDone" data-dismiss="modal">Close</button>
           
            <button id="send" type="button" class="btn btn-primary">Send</button>
           
          </div>
        </div>
      </div>
    </div>
    <script>

        
        $("#recordDone").click(function() {
            $('#modal_ticket').val(''); 
            $('#recordContent').html('');
        });
        
        $("#send").click(function() {
            $(".lBar").removeClass('hidden');
            $.post( "{{url('/api/ticket/record/update')}}", {
                'event_id': $('#event_id').val(),
                'event_period': $('#chevent').val(),
                },
                function( data ) {
                    $(".lBar").addClass('hidden'); 
            });
        });
    </script>
@endsection