@extends('template.main') 
@section('addCSS')
    <style>
        .lBar{
            z-index: 9999;
        }
    </style>
    
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
@endsection

@section('content')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<div class="card">
    <div class="card-header">
        <strong class="card-title">Events</strong>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <form action="{{url('/event/create')}}" method="post" id="createForm" style="padding: 20px;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="form-group col-md-12">
                <label for="Name">Name of Event</label>
                <input type="text" class="form-control" name="eventname" id="eventname" placeholder="Event Name" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="Name">In Game Event ID</label>
                <input type="text" class="form-control" name="in_game_event_id" id="in_game_event_id" placeholder="In Game Event ID" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="Name">Period of Event</label>
                <input type="text" class="form-control" name="chevent" id="chevent">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <button type="button" id="btnchevent" class="col-md-12 btn btn-primary rounded">Submit</button>
            </div>
        </div>
    </form>
</div>
<!-- <div class="card" style="background-color:red;">
    <div class="card-header">
        <strong class="card-title">List of Events</strong>
    </div>
    <div class="card-body" style="background-color:blue;">
        <table class="table table-hover table-bordered table-striped datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Start</th>
                    <th>End</th>
                </tr>
            </thead>
        </table>
    </div>
</div> -->
<script src="https://datatables.yajrabox.com/js/bootstrap.min.js"></script>
<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
<script type="text/javascript">

    $(function() {
      $('input[name="chevent"]').daterangepicker({
        timePicker: true,
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(32, 'hour'),
        locale: {
          format: 'M/DD hh:mm A'
        }
      });
      

        // setup submission on some random button
      $('#btnchevent').click(function() { 
        // alert($('#shift').val());
            $(".lBar").removeClass('hidden');
            $.ajax({
            url: "/event/create",
            type:'post',
            data:{
                    chevent : $('#chevent').val(),
                    _token:$('#token').val(),
                    name:$('#eventname').val(),
                    in_game_event_id:$('#in_game_event_id').val(),
                },
            success: function (data) {

                $(".lBar").addClass('hidden');

                // $(".msg").html("Success");
            },
        });
        });
     
    });
</script>
@endsection