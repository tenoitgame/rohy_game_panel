<!DOCTYPE html>
<html >
    <head>
        <meta charset="utf-8" />
        <title>Event</title>
        
        <!-- The stylesheet -->
        <style type="text/css">
        	
			html,
			body,
			.container {
			  height: 100%;
			  width: 100%;
			}
			.container {
			  display: flex;
			  align-items: center;
			  justify-content: center;
			  
			}
			.mydiv {
			  background-color: blue;
			  max-width: 950px;
			  margin: 0 auto;
			  /*background:url('{{$event[0]->banner_img}}') no-repeat center center;*/
			}
        </style>
        
        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    
    <body>
    	<button onclick="goBack()">Go Back</button>
    	
    	<div class="container">
		  <div class="mydiv">{{$event[0]->name}}<img onclick="goLink()" src="{{$event[0]->banner_img}}"></div>
		</div>
        <footer>
	       
        </footer>
        
    </body>
    <script>
	// On mouse-over, execute myFunction
	function goLink() {
	    window.location.href = '/en/event/{{$event[0]->id}}';
	}
	function goBack() {
	    window.history.back();
	}
	</script>
</html>