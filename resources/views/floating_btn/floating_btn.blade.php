@extends('template.main') 
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
    table { table-layout: fixed; }
    table th, table td { overflow: hidden; padding: 10px;}
</style>
@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Floating Button</strong>
        <div class="row">
            <a href="#addFloatingBtnModal" class="btn btn-success rounded" data-toggle="modal">
                <span>Add Btn Control</span>
            </a>       
        </div>
    </div>
    <div class="card-body">
        <table id="floatingBtn" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>GameCode</th>
                    <th>Version</th>
                    <th>Status</th>
                    <th>CreateTime</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var table = $('#floatingBtn').DataTable( {
        lengthChange: false,
        ajax: "{{ '/api/floatingBtn/get' }}",
        columns: [
            { data: "gameCode", width: "15%"},
            { data: "version", width: "10%" },
            { data: "status", width: "10%", render : function (data, type, row) {
                return data == 1 ? '開' : '關';
            }},
            { data: "createTime", width: "10%" },
            { data: null, defaultContent:"<a href='#editFloatingBtnModal' class='edit btn btn-primary rounded' data-toggle='modal'><span>Edit</span></a>","width": "10%"},
        ],
        select: true
    });

    $('#floatingBtn tbody').on( 'click', 'a.edit', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        $("#id").val(data['id']);
        $("#GameCode").val(data['gameCode']);
        $("#Version").val(data['version']);
        $("#Status").val(data['status']);
    });
});
</script>
@endsection

@section('modal')
<div id="addFloatingBtnModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/floatingBtn/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Add Game</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>GameCode</label>
                        <input type="text" class="form-control" name="GameCode" required>
                    </div>
                    <div class="form-group">
                        <label>Version</label>
                        <input type="text" class="form-control" name="Version" required>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        {{-- <input type="text" class="form-control" name="Status" required> --}}
                        <select class="form-control" name="Status">
                            <option value='1'>開</option>
                            <option value="2">關</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('modal2')
<div id="editFloatingBtnModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/floatingBtn/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Game</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input id="id" name="id" required style="display: none;">
                    <div class="form-group">
                        <label>GameCode</label>
                        <input type="text" class="form-control" id="GameCode" name="GameCode" required>
                    </div>
                    <div class="form-group">
                        <label>Version</label>
                        <input type="text" class="form-control" id="Version" name="Version" required>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        {{-- <input type="text" class="form-control" id="Status" name="Status" required> --}}
                        <select class="form-control" id="Status" name="Status">
                            <option value='1'>開</option>
                            <option value="2">關</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection