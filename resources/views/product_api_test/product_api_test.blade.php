@extends('template.main') @section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Create User</strong>
    </div>
    <form action="{{url('/productTest/test')}}" method="post" id="createForm" style="padding: 20px;" enctype="multipart/form-data">
        <div class="row">
            <div class="form-group col-md-2">
                <label for="gameName">Role</label>
                <select name="gameName" id="gameName" class="form-control" required>
                    <option value="ch">ch</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="name">Excel</label>
                <input type="file" id="csvFile" name="csvFile">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-2">
                <label for="role">UserId</label>
                <input type="text" class="form-control" id="UserId" name="UserId" value="370">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-2">
                <label for="role">Role</label>
                <input type="text" class="form-control" id="Role" name="Role" value="215289554">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-2">
                <label for="role">Server</label>
                <input type="text" class="form-control" id="Server" name="Server" value="9001">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-primary rounded">Test</button>
            </div>
        </div>
    </form>
    <div class="card-body">
        <table id="game" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ProductId</th>
                    <th>ItemId</th>
                    <th>ResultCode</th>
                    <th>ResultMessage</th>
                </tr>
                @if(session()->has('data'))
                    @foreach(session()->get('data') as $row);
                    <tr>
                        <td>{{ $row['productId'] }}</td>
                        <td>{{ $row['itemId'] }}</td>
                        <td>{{ $row['result']->code }}</td>
                        <td>{{ property_exists($row['result'], "msg") ? $row['result']->msg : $row['result']->message }}</td>
                    </tr>
                    @endforeach
                @endif

            </thead>
        </table>
    </div>
</div>
@endsection