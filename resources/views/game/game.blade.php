@extends('template.main') 
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
    table { table-layout: fixed; }
    table th, table td { overflow: hidden; padding: 10px;}
</style>
@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Game</strong>
        <div class="row">
            <a href="#addGameModal" class="btn btn-success rounded" data-toggle="modal">
                <span>Add New Game</span>
            </a>       
        </div>
    </div>
    <div class="card-body">

        <table id="game" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>GameName</th>
                    <th>GameShortCode</th>
                    <th>GameCode</th>
                    <th>Android PackageName</th>
                    <th>ItunesAppId</th>
                    <th>CreateTime</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var table = $('#game').DataTable( {
        lengthChange: false,
        ajax: "{{ '/api/game/get' }}",
        columns: [
            { data: "gameName", width: "15%"},
            { data: "gameShortCode", width: "10%" },
            { data: "gameCode", width: "10%" },
            { data: "package_name", width: "15%" },
            { data: "itunes_app_Id", width: "15%" },
            { data: "createdTime", width: "10%" },
            { data: null, defaultContent:"<a href='#editGameModal' class='edit btn btn-primary rounded' data-toggle='modal'><span>Edit</span></a>","width": "10%"},
        ],
        select: true
    });

    $('#game tbody').on( 'click', 'a.edit', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        $("#id").val(data['gid']);
        $("#GameName").val(data['gameName']);
        $("#GameShortCode").val(data['gameShortCode']);
        $("#GameCode").val(data['gameCode']);
        $("#AndroidPackageName").val(data['package_name']);
        $("#ItunesAppId").val(data['itunes_app_Id']);
       	$("#loginAllow").val(data['loginAllow']);
    });
});
</script>
@endsection

@section('modal')
<div id="addGameModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/game/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Add Game</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>GameName</label>
                        <input type="text" class="form-control" name="GameName" required>
                    </div>
                    <div class="form-group">
                        <label>GameShortCode</label>
                        <input type="text" class="form-control" name="GameShortCode" required>
                    </div>
                    <div class="form-group">
                        <label>GameCode</label>
                        <input type="text" class="form-control" name="GameCode" required>
                    </div>
                    <div class="form-group">
                        <label>Android PackageName</label>
                        <input type="text" class="form-control" name="AndroidPackageName" >
                    </div>
                    <div class="form-group">
                        <label>ItunesAppId</label>
                        <input type="text" class="form-control" name="ItunesAppId" >
                    </div>
                    <div class="form-group">
                        <label>Server Maintance<</label>
                        <select class="form-control" name="loginAllow">
                            <option selected value="N">YES</option>
                            <option value="Y">NO</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('modal2')
<div id="editGameModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/game/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Game</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input id="id" name="id" required style="display: none;">
                    <div class="form-group">
                        <label>GameName</label>
                        <input type="text" class="form-control" id="GameName" name="GameName" required>
                    </div>
                    <div class="form-group">
                        <label>GameShortCode</label>
                        <input type="text" class="form-control" id="GameShortCode" name="GameShortCode" required>
                    </div>
                    <div class="form-group">
                        <label>GameCode</label>
                        <input type="text" class="form-control" id="GameCode" name="GameCode" required>
                    </div>
                    <div class="form-group">
                        <label>Android PackageName</label>
                        <input type="text" class="form-control" id="AndroidPackageName" name="AndroidPackageName" >
                    </div>
                    <div class="form-group">
                        <label>ItunesAppId</label>
                        <input type="text" class="form-control" id="ItunesAppId" name="ItunesAppId" >
                    </div>
                   <div class="form-group">
                        <label>Server Maintance</label>
                        <select class="form-control" id="loginAllow" name="loginAllow">
                            <option value="N">YES</option>
                            <option value="Y">NO</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection