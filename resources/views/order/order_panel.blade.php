@extends('template.main') 
@section('addCSS')
    <style>
        .lBar{
            z-index: 9999;
        }
    </style>
    
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
@endsection

@section('content')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<div class="card">
    <div class="card-header">
        <strong class="card-title">Events</strong>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <form action="{{url('/event/create')}}" method="post" id="createForm" style="padding: 20px;">
        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="form-group col-md-9">
                <label for="Name">Name of Account</label>
                <select name="account" id="account" class="selectpicker form-control">
                    <?php foreach ($users as $obj) {
                    ?>
                    <option value="<?php echo $obj->uid;?>" ><?php echo $obj->username;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-3">
                <label for="Name" style="color:white;"> .</label>
                <button type="button" id="searchorder" class="col-md-12 btn btn-primary rounded">Search</button>
            </div>
        </div>
        <div class="row">
            <table class="table" id="ticket-table">
                <thead>
                    <tr>
                        <th scope="col">Order no.</th>
                        <th scope="col">Account of.</th>
                        <th scope="col">Status</th>
                        <th scope="col">Change</th>
                    </tr>
                </thead>
                <tbody id="content">
                    <tr>
                        
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <button type="button" id="btnchevent" class="col-md-12 btn btn-primary rounded">Submit</button>
            </div>
        </div>
    </form>
</div>

<script src="https://datatables.yajrabox.com/js/bootstrap.min.js"></script>
<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
<script type="text/javascript">

        // setup submission on some random button
      $('#searchorder').click(function() { 
        loadTable();
            
        });

        function loadTable(){
            $(".lBar").removeClass('hidden');
            $.ajax({
            url: "/api/order/search",
            type:'post',
            data:{
                    account : $('#account').val(),
                    _token:$('#token').val()
                },
            success: function (data) {

                $(".lBar").addClass('hidden');

                var list = data;
                    var line = "<tr><td>{0}</td><td>{1}</td><td>{3}</td><td><button type='button' class='testing col-md-12 btn btn-primary rounded' id='{2}' onClick='changeStatus(this)'>Test</button></td></tr>";
                    var board = $('#content');
                    board.html('');
                    list.forEach(function(item) {
                        var tmp = line;
                        tmp = tmp.replace('{0}', item['rohy_order_no']);
                        tmp = tmp.replace('{1}', item['user_uid']);
                        tmp = tmp.replace('{2}', item['uid']);
                        tmp = tmp.replace('{3}', item['order_status']);
                        board.append(tmp);
                    });
            },
        });
        }
    function changeStatus(elem){
        // alert(elem.id);
        $.post( "/api/order/change",
            {
                idorder: elem.id,
                _token: '{{ csrf_token() }}',
            },
            function(data) {
                loadTable();
            }
        );
      }

</script>
<script type="text/javascript">
    
</script>
@endsection