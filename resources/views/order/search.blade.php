@extends('template.main')

@section('content')
	<div class="card">
		<div class="card-header">
             <strong class="card-title">Search Order</strong>
        </div>
        <nav class="panel panel-default well">
        	<div class="row" style="padding:15px;">
         		<div class="col-sm-2">Search Period:</div>
    			<div class="col-sm-3">
    				<input class="datepicker datetimepickerStart" data-date-format="yyyy-mm-dd">
        		</div>
        		<div class="col-sm-1">TO</div>
        		 <div class="col-sm-3">
        		 	<input class="datepicker datetimepickerEnd" data-date-format="yyyy-mm-dd">
        		</div>
            </div>
        	<div class="row" style="padding:15px;">
        		<div class="col-sm-2">Status:</div>
    			<div class="form-check col-sm-2">
					<input type="checkbox" class="form-check-input" id="statusCM" checked>
                    <label class="form-check-label" for="statusCM">COMPLETED</label>
           		</div>
                <div class="form-check col-sm-2">
					<input type="checkbox" class="form-check-input" id="statusCL" checked>
                    <label class="form-check-label" for="statusCL">CANCEL</label>
                </div>
                <div class="form-check col-sm-2">
                    <input type="checkbox" class="form-check-input" id="statusE" checked>
                    <label class="form-check-label" for="statusE">ERROR</label>
                </div>
                <div class="form-check col-sm-2">
					<input type="checkbox" class="form-check-input" id="statusW" checked>
                    <label class="form-check-label" for="statusW">WAITING</label>
                </div>
        	</div>
        	<div class="row" style="padding:15px;">
        		<div class="col-sm-2">Game:</div>
        		<div class="form-group col-sm-3">
        			<select class="form-control" id="GameNameInput">
        				<option value=''>ALL</option>
						<option value="trsm">TRSM</option>
						<option value="ch">Crystal Heart</option>
                    </select>
                </div>
        	</div>
			<div class="row" style="padding:15px;">
				<div class="col-sm-2">Account Type:</div>
				<div class="form-group col-sm-2">
        			<select class="form-control" id="AccountTypeInput">
        				<option value="">N/A</option>
                    	<option value="NORMAL">ROHYSEA</option>
						<option value="GOOGLE">GOOGLE</option>
						<option value="FACEBOOK">FACEBOOK</option>
                    </select>
                </div>
				<div class="col-sm-2">Account Name:</div>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="AccountNameInput" placeholder="Gamer11">
				</div>
			</div>
            <div class="row" style="padding:15px;">
                <div class="col-sm-6">
                	<button type="button" id="search" class="btn btn-primary">Search</button>
                	<button type="button" id="clear" class="btn btn-primary">Clear</button>
                </div>
            </div>
        </nav>
        <div class="card-body">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">Order no.</th>
						<th scope="col">Game</th>
						<th scope="col">AccountId</th>
						<th scope="col">AccountName</th>
						<th scope="col">Type</th>
						<th scope="col">Price</th>
						<th scope="col">Status</th>
						<th scope="col">Create Time</th>
					</tr>
				</thead>
				<tbody id="content">
				</tbody>
			</table>
			<div class="row">
            	<ul id="pagination" class="pagination"></ul>
            </div>
        </div>
        <div id="hidden_data" style="display:none">
            <input id="fromPage" value="1">
            <input id="token" value="{{ csrf_token() }}">
            <input id="startTime" value="">
            <input id="endTime" value="">
            <input id="orderStatus" value=15>
            <input id="accountType" value="">
            <input id="accountName" value="">
            <input id="gameName" value="">
		</div>
	</div>
	<script>
       	function pageSearch(index){

           	var statuFlag = $('#orderStatus').val();
           	var statusType = [];
			if (statuFlag & 1) {
				statusType.push('COMPLETED');
			}
			if (statuFlag & 2) {
				statusType.push('CANCEL');
			}
			if (statuFlag & 4) {
				statusType.push('ERROR');
			}
			if (statuFlag & 8) {
				statusType.push('WAITING');
			}
			$(".lBar").removeClass('hidden');
    		$.post( "{{url('api/order/searchData')}}", {
    			'index': index,
    			'startTime': $('#startTime').val(),
    			'endTime': $('#endTime').val(),
    			'orderStatus': statusType,
    			'gameName': $('#GameNameInput').val(),
    			'accountType': $('#accountType').val(),
    			'accountName': $('#accountName').val(),
    			'_token': $('#token').val()
    			},
    			function( data ) {
    				$(".lBar").addClass('hidden');
    				appendContent(data['list']);
            		appendPaging(data["pagination"]);
            	
    		});
    	}

    	function appendContent(list) {
    		$("#content").html('');
   		 	var td_string = "<td>{0}</td>";
        	for (index in list) {
        		 $('#content').append(
		            "<tr id='"+list[index]['rohy_order_no']+"'>" +
                    "<td class='orderinfo'>"+list[index]['rohy_order_no']+"</td>" +
		            td_string.format(list[index]['game_name']) +
		            td_string.format(list[index]['userid']) +
		            td_string.format(list[index]['userName']) +
		            td_string.format(list[index]['type']) +
		            "<td class='priceinfo'>"+list[index]['currency'] + ' ' + list[index]['paid_amount']+"</td>" +
		            td_string.format(list[index]['order_status']) +
		            td_string.format(list[index]['create_time']) +
		         "</tr>");
        	}
		}
		$('#clear').on('click', function(){
			$('#statusCM').prop("checked", true);
			$('#statusE').prop("checked", true);
			$('#statusCL').prop("checked", true);
			$('#statusW').prop("checked", true);
            $('.datetimepickerStart').val(''),
            $('.datetimepickerEnd').val(''),
			$('#GameNameInput').val('');
			$('#AccountTypeInput').val('');
			$('#AccountNameInput').val('');
		});
		
		$('#search').on('click', function(){
			var startTime = $('.datetimepickerStart').val();
			var endTime = $('.datetimepickerEnd').val();
			
			if (startTime == '' || endTime == '') {
				swal('You must pick time period');
				return;
			}

			var accountType = $('#AccountTypeInput').val();
			var accountName = $('#AccountNameInput').val();
			if (accountType != '' && accountName == '') {
				swal('You must give account name argument');
				return;
			}
			
			$('#startTime').val(startTime);
			$('#endTime').val(endTime);

			var typeFlag = 0;
			typeFlag += $('#statusCM').is(":checked") ? 1 : 0;
            typeFlag += $('#statusCL').is(":checked") ? 2 : 0;
			typeFlag += $('#statusE').is(":checked") ? 4 : 0;
			typeFlag += $('#statusW').is(":checked") ? 8 : 0;
			if (typeFlag == 31) {
				typeFlag = 0;
			};
			
			$('#orderStatus').val(typeFlag);
			$('#gameName').val($('#GameNameInput').val());

	
			$('#accountType').val(accountType);
			$('#accountName').val(accountName);
			pageSearch(1, startTime, endTime);
		});

		// $(document).on('click', '.priceinfo', function() {
		// 	var id=$(this).closest('tr').attr('id');
		// 	getPriceInfo(id);
		// });

        // $(document).on('click', '.orderinfo', function() {
        //     var id=$(this).closest('tr').attr('id');
        //     getOrderInfo(id);
        // });

        $(document).on('click', '.totest', function() {
            var id=$(this).closest('tr').attr('id');
            toTest(id);
        });

		function pop() {
			// alert('halo');
			$('#recordModal').modal('show');
		}

		function getPriceInfo(id) {
			// alert(id);
    		$('#modal_ticket').val(id); 
    		$(".lBar").removeClass('hidden');
       		$.post( "{{url('api/order/record/collect')}}", {
    			'id': id,
    			"_token": $('#token').val()
    			},
    			function( data ) {
    				$(".lBar").addClass('hidden');
					var list = data['list'];
    				var line = "<tr><td><div class='{0} msg'><div class='title'>{1}</div><div class='body'>{2}</div><div>{3}</div></div></td></tr>";
    	    		var board = $('#recordContent');
    	    		board.html('');

    	    		
    	    			var tmp = line;
    	    			tmp = tmp.replace('{0}', list.rohy_order_no);
    	        		tmp = tmp.replace('{1}', list.game_name);
    	        		tmp = tmp.replace('{2}', list.finalprice);
    	    			tmp = tmp.replace('{3}', list.create_time);
    	    			board.append(tmp);
    	    		
    	    		$('#recordModal').modal('show');
    
    	    		setTimeout(function() {
    	    			$("#recordModal .modal-body").scrollTop($("#recordModal .modal-body")[0].scrollHeight);

                        var recordContent = document.getElementById("recordContent");
                        recordContent.scrollTop = recordContent.scrollHeight;
    	    		}, 500);
	    			$('#recordModal').css('zIndex', 9999);
	    			$('#recordModal').css('position', 'absolute');
    	    		
    		});
    	}

        function getOrderInfo(id) {
            $('#modal_ticket').val(id); 
            $(".lBar").removeClass('hidden');
            $.post( "{{url('api/order/verifyRequest')}}", {
                'orderNo': id,
                "_token": $('#token').val()
                },
                function( data ) {
                    $(".lBar").addClass('hidden');
                    var line = "<div>{0}</div><div>{1}</div>";
                    var board = $('#recordContent');
                    board.html('');

                    var tmp = line;
                    tmp = tmp.replace('{0}', data.err);
                    tmp = tmp.replace('{1}', data.message);
                    board.append(tmp);
                    
                    $('#recordModal').modal('show');
                    $('#recordModal').css('zIndex', 9999);
                    $('#recordModal').css('position', 'absolute');
                    
            });
        }

        function toTest(id) {
            $('#modal_ticket').val(id); 
            $(".lBar").removeClass('hidden');
            $.post( "{{url('api/order/toTestOrder')}}", {
                'orderNo': id,
                "_token": $('#token').val()
                },
                function( data ) {
                    $(".lBar").addClass('hidden');
                    $('#search').click();
                    
            });
        }

    	$(document).ready(function($) {
    		$('.datepicker').datepicker();
    	});
    	
	</script>
@endsection
@section('modal')
	<div class="modal fade" id="recordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
      <div class="modal-dialog" role="document" style="max-width:800px">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Record</h5>
            <button type="button" class="close recordDone" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        	<div id="hidden_data" style="display:none">
            	<input id="modal_ticket" value="1">
                <input id="request_id" value="-1">
			</div>
          <div class="modal-body modal-frame">
          	
            <div style="display:inline-block;">
              	<table style="border-collapse:collapse;border:none;width:100%;">
                	<tbody id="recordContent" style="display:block;overflow: auto;height: 250px;">
    				</tbody>
    				<!-- <tfoot>
    					<tr>
    						<td style="padding:10px 0px 0px 0px;">
    							<div class="msg_text"><textarea id="msg"></textarea></div>
    						</td>
    					</tr>
    					<tr>
    						<td>
    							<button id="send" type="button" class="btn btn-primary">Send</button>
    						</td>
              			</tr>
    				</tfoot>
 -->                </table>
          	</div>
          </div>
          <div class="modal-footer">
        	<!-- <div id="RequestPM">
    			<select class="form-control" id="RequestType" style="width: 180px; display: inline-block;">
    				<option value="">N/A</option>
                	<option value="REWARD">Re-clamp reward</option>
					<option value="ORDER">Order Issues</option>
					<option value="REFUND">Refund Payment</option>
					<option value="OTHERS">Others</option>
                </select>
          		<button id="request" type="button" class="btn btn-info">PM Request</button>
          	</div> -->
          </div>
        </div>
      </div>
    </div>
@endsection