@extends('template.main') 
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
    table { table-layout: fixed; }
    table th, table td { overflow: hidden; padding: 10px;}
</style>
@section('content')
<div class="card">
    @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="card-header">
        <strong class="card-title">Exchange Event</strong>
    </div>
    <div class="card-body">
        <table id="exchangeRecords" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>GameName</th>
                    <th>UserId</th>
                    <th>ServerCode</th>
                    <th>RoleId</th>
                    <th>SerialNo</th>
                    <th>ExchangeType</th>
                    <th>ExchangeFrom</th>
                    <th>Status</th>
                    <th>CreateTime</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var table = $('#exchangeRecords').DataTable( {
        lengthChange: false,
        ajax: "{{ '/api/exchangeRecords/get' }}",
        columns: [
            { data: "id", width: "5%"},
            { data: "game_name", width: "5%" },
            { data: "userid", width: "5%" },
            { data: "server_code", width: "5%" },
            { data: "role_id", width: "10%" },
            { data: "serial_no", width: "10%" },
            { data: "exchange_type", width: "10%" },
            { data: "exchange_from", width: "10%" },
            { data: "status", width: "10%" },
            { data: "create_time", width: "10%" },
            { data: null, defaultContent:"<a href='#detailGameModal' class='detail btn btn-primary rounded' data-toggle='modal'><span>Detail</span></a>","width": "10%"},
        ],
        select: true
    });

    var tableExchangeLogs;
    $('#exchangeRecords tbody').on( 'click', 'a.detail', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        if(tableExchangeLogs != null) {
            tableExchangeLogs.destroy();
        }
        tableExchangeLogs = $('#exchangeLogs').DataTable( {
            lengthChange: false,
            ajax: {
                "url" : "{{ '/api/exchangeRecords/logs/get' }}",
                "data": function ( d ) {
                    d.game_exchange_id = data['id'];
                }
            },
            columns: [
                { data: "id", width: "10%"},
                { data: "message", width: "50%" },
                { data: "status", width: "10%" },
                { data: "create_time", width: "10%" },
            ],
        });
    });
});
</script>
@endsection

@section('modal')
<div id="detailGameModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="padding: 20px">
            <table id="exchangeLogs" class="table" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Message</th>
                        <th>Status</th>
                        <th>CreateTime</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection