@extends('template.main')

@section('content')
	<div class="card">
		<div class="card-header">
             <strong class="card-title">Ban List</strong>
        </div>
   		<nav class="panel panel-default well">
			<div class="row" style="padding:15px;">
        		<div class="col-sm-2">Ban Type:</div>
        		<div class="form-group col-sm-3">
        			<select class="form-control" id="BanTypeInpt">
        				<option value='all'>Account</option>
						<option value="trsm">TRSM</option>
						<option value="ch">Crystal Heart</option>
                    </select>
                </div>
        	</div>
        </nav>
        <div class="card-body">
			<table class="table">
	            <thead>
                    <tr>
                        <th scope="col">User ID</th>
                        <th scope="col">Account Name</th>
                        <th scope="col">Facebook ID</th>
                        <th scope="col">Googel ID</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Create Time</th>
                        <th scope="col">Banned</th>
                    </tr>
                </thead>
				<tbody id="content">
				</tbody>
			</table>
			<div class="row">
            	<ul id="pagination" class="pagination"></ul>
            </div>
        </div>
        <div id="hidden_data" style="display:none">
            <input id="fromPage" value="1">
            <input id="token" value="{{ csrf_token() }}">
		</div>
	</div>

	<script>
		$("#BanTypeInpt").change(function() {
			pageSearch(1);
		});

       	function pageSearch(index){
       		$(".lBar").removeClass('hidden');
    		$.post( "{{url('api/account/ban/list')}}", {
    			'index': index,
    			'banType': $('#BanTypeInpt').val(),
    			"_token": $('#token').val()
    			},
    			function( data ) {
    				$(".lBar").addClass('hidden');
    				appendContent(data['list']);
            		appendPaging(data["pagination"]);
    		});
    	}

       	function unlock(uid) {
       		$.post( "{{url('api/account/ban/unlock')}}", {
    			'uid': uid,
    			'banType': $('#BanTypeInpt').val(),
    			"_token": $('#token').val()
    			},
    			function( data ) {
        			swal(data['msg']);
    				pageSearch(1);
    		});
       	}
    	function appendContent(list) {
    		$("#content").html('');
   		 	var td_string = "<td>{0}</td>";
   	    	var btn_string = "<td><button class='btn btn-primary' onclick='unlock({0})'>unlock</button></td>";
       	    for (index in list) {
       	        var userName = list[index]['userName'] == null ? '-' : list[index]['userName'];
       	        var facebook_id = list[index]['facebook_id'] == null ? '-' : list[index]['facebook_id'];
       	        var google_id = list[index]['google_id'] == null ? '-' : list[index]['google_id'];
       	        var email = list[index]['email'] == null ? '-' : list[index]['email'];
       	        $('#content').append(
       	            "<tr>" +
       	            td_string.format(list[index]['uid']) +
       	            td_string.format(userName) +
       	            td_string.format(facebook_id) +
       	            td_string.format(google_id) +
       	            td_string.format(email) +
       	            td_string.format(list[index]['registerTime']) +
       	         	btn_string.format(list[index]['uid']) +
       	            "</tr>");
       	    }
		}
		
    	$(document).ready(function($) {
			pageSearch(1);
    	});
	</script>
@endsection