@extends('template.main')

@section('content')
<div class="card">
		<div class="card-header">
             <strong class="card-title">Ban Account</strong>
        </div>
        <nav class="panel panel-default well">
			<div class="row" style="padding:15px;">
				<div class="col-sm-2">Account Type:</div>
				<div class="form-group col-sm-3">
        			<select class="form-control" id="AccountTypeInput">
        				<option value="">Please Choose</option>
        				<option value="UID">ID</option>
                    	<option value="NORMAL">ROHYSEA</option>
						<option value="GOOGLE">GOOGLE</option>
						<option value="FACEBOOK">FACEBOOK</option>
                    </select>
                </div>
				<div class="col-sm-2">Account Name:</div>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="AccountNameInput" placeholder="Please enter id or name">
				</div>
			</div>
			<div class="row" style="padding:15px;">
                <div class="col-sm-6">
                	<button type="button" id="search" class="btn btn-info">Search</button>
                </div>
            </div>
			<div class="row" style="padding:15px;">
        		<div class="col-sm-4">Ban from a game or account:</div>
        		<div class="form-group col-sm-3">
        			<select class="form-control" id="BanTypeInpt" disabled>
        				<option value=''>Please Choose</option>
						<option value="trsm">TRSM</option>
						<option value="ch">Crystal Heart</option>
						<option value='all'>Account</option>
                    </select>
                </div>
        	</div>
        	
            <div class="row" style="padding:15px;">
                <div class="col-sm-6">
                	<button type="button" id="submit" class="btn btn-primary" disabled>Submit</button>
                	<button type="button" id="cancel" class="btn btn-danger" disabled>Cancel</button>
                </div>
            </div>
        </nav>
        <div id="hidden_data" style="display:none">
            <input id="uid" value="">
		</div>
	</div>

	<script>
    	$('#search').on('click', function(){
       		var type = $('#AccountTypeInput').val();
       		var name = $('#AccountNameInput').val();
       		if (type == '' || name == '') {
    			swal('Data not completed');
    			return;
       		}
       		$(".lBar").removeClass('hidden');
    		$.post( "{{url('api/account/check')}}", {
    			'accountType': type,
    			'accountName': name,
    			'_token': $('#token').val()
    			},
    			function( data ) {
    				$(".lBar").addClass('hidden');
                	if (data['err'] == 0) {
                    	$('#AccountTypeInput').prop('disabled', true);
                    	$('#AccountNameInput').prop('disabled', true);
                      	$('#search').prop('disabled', true);
                    	$('#BanTypeInpt').prop('disabled', false);
                      	$('#cancel').prop('disabled', false);
                    	$('#submit').prop('disabled', false);
                    	$('#uid').val(data['data']['uid']);
                	}
                	
                	swal(data['msg']);
    		});
    	});
    	$('#cancel').on('click', function(){
         	$('#AccountTypeInput').prop('disabled', false);
        	$('#AccountNameInput').prop('disabled', false);
          	$('#search').prop('disabled', false);
          	$('#BanTypeInpt').val('');
        	$('#BanTypeInpt').prop('disabled', true);
          	$('#cancel').prop('disabled', true);
        	$('#submit').prop('disabled', true);
        	$('#userid').val('');
    	});
    	
		$('#submit').on('click', function(){

       		var uid = $('#uid').val();
       		var ban = $('#BanTypeInpt').val();
       		if (uid == '' || ban == '') {
				swal('Data not completed');
				return;
       		}
       		$(".lBar").removeClass('hidden');
    		$.post( "{{url('api/account/ban')}}", {
    			'banType': ban,
    			'uid': uid,
    			'_token': $('#token').val()
    			},
    			function( data ) {
    				$(".lBar").addClass('hidden');
            		swal(data['msg']);
            		if (data['err'] == 0) {
             			$('#AccountTypeInput').val('');
            			$('#AccountNameInput').val('');
            			$('#AccountTypeInput').prop('disabled', false);
                    	$('#AccountNameInput').prop('disabled', false);
                      	$('#search').prop('disabled', false);
                      	$('#BanTypeInpt').val('');
                    	$('#BanTypeInpt').prop('disabled', true);
                      	$('#cancel').prop('disabled', true);
                    	$('#submit').prop('disabled', true);
                    	$('#userid').val('');
            		}
    		});
		});
	</script>
@endsection