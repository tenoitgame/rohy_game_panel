@extends('template.main') 
<style>
    .lBar{
        z-index: 9999;
    }
    table { table-layout: fixed; }
    table th, table td { overflow: hidden; }
</style>
@section('addCSS')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Search Account</strong>
    </div>
    <nav class="panel panel-default well">
        <div class="row" style="padding:15px;">
            <div class="col-sm-2">User ID:</div>
            <div class="col-sm-2 form-group">
                <input type="text" class="form-control" id="userIdInput" placeholder="123456">
            </div>
            <div class="col-sm-2">Account Name:</div>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="accountNameInput" placeholder="Gamer11">
            </div>
        </div>
        <div class="row" style="padding:15px;">
            <div class="col-sm-2">Status:</div>
            <div class="form-group form-check col-sm-2">
                <input type="checkbox" class="form-check-input" id="statusNormal" checked>
                <label class="form-check-label" for="statusNormal">Normal</label>
            </div>
            <div class="form-check col-sm-2">
                <input type="checkbox" class="form-check-input" id="statusBanned" checked>
                <label class="form-check-label" for="statusBanned">Banned</label>
            </div>
        </div>
        <div class="row" style="padding:15px;">
            <div class="col-sm-6">
                <button type="button" id="search" class="btn btn-primary">Search</button>
                <button type="button" id="clear" class="btn btn-primary">Clear</button>
            </div>
        </div>
    </nav>
    <div class="card-body">
        <table class="table" id="ticket-table">
            <thead>
                <tr>
                    <th scope="col">User ID</th>
                    <th scope="col">Account Name</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Create Time</th>
                    <th scope="col">Can Login</th>
                </tr>
            </thead>
        </table>
    </div>
    <div id="hidden_data" style="display:none">
        <input id="fromPage" value="1">
        <input id="userId" value="">
        <input id="accountName" value="">
        <input id="accountStatus" value="">
        <input id="token" value="{{ csrf_token() }}">
    </div>
</div>
<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
<script>
var table

function search() {
    var statuFlag = $('#accountStatus').val();
    var statusType = [];
    if (statuFlag & 1) {
        statusType.push('Y');
    }
    if (statuFlag & 2) {
        statusType.push('N');
    }
    
    $("#ticket-table").dataTable().fnDestroy()
    table = $('#ticket-table').DataTable({
        processing: false,
        serverSide: false,
        rowId: 'id',
        ajax: {
            url : "{{ url('api/account/search') }}",
            type : "POST",
            data: function (d) {
                d.userId = $('#userId').val(),
                d.accountName = $('#accountName').val(),
                d.accountStatus = statusType,
                d._token = $('#token').val()
            },
        },
        columns:[
            {data:'uid',"width": "10%"},
            {data: 'userName',"width": "25%", render : function (data, type, row) {
                return data == null ? '-' : data;
            }},
            {data: 'email',"width": "20%", render : function (data, type, row) {
                return data == null ? '-' : data;
            }},
            {data:'registerTime',"width": "15%"},
            {data:'flag',"width": "10%"},
        ],
    });
}

$('#clear').on('click', function() {
    $('#userIdInput').val('');
    $('#accountNameInput').val('');
    $('#statusNormal').prop("checked", true);
    $('#statusBanned').prop("checked", true);
});

$('#search').on('click', function() {
    var typeFlag = 0;
    typeFlag += $('#statusNormal').is(":checked") ? 1 : 0;
    typeFlag += $('#statusBanned').is(":checked") ? 2 : 0;
    if (typeFlag == 3) {
        typeFlag = 0;
    };

    $('#userId').val($('#userIdInput').val());
    $('#accountName').val($('#accountNameInput').val());
    $('#accountStatus').val(typeFlag);
    
    search();
});

$(document).ready(function($) {
    $('.datepicker').datepicker();
});
</script>
@endsection @section('modal')
<style>
.CLIENT {
    color: #333;
    font-size: 12px;
    max-width: 70%;
    border-radius: 3px;
    border: 1px solid #000;
    margin-bottom: 10px;
}

.CS {
    color: #333;
    font-size: 12px;
    margin-left: 30%;
    max-width: 70%;
    border-radius: 3px;
    border: 1px solid #CCC;
    background: #CCC;
    margin-bottom: 10px;
}

.msg div:nth-child(1) {
    margin: 5px 15px;
    overflow: hidden;
    white-space: nowrap
}

.msg div:nth-child(2) {
    margin: 0px 15px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    font-size: 10px;
    color: #666
}

.msg div:nth-child(3) {
    margin: 5px 15px;
    text-align: right
}

.msg_text {
    height: 100px;
}

.msg_text textarea {
    padding: 5px;
    height: 80px;
    width: 100%;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    border-radius: 5px;
}
</style>
<div class="modal fade" id="recordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close recordDone" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="hidden_data" style="display:none">
                <input id="modal_ticket" value="1">
            </div>
            <div class="modal-body">
                <table style="width:100%; border-collapse: collapse;border: none;">
                    <tbody id="recordContent">
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="padding:10px 0px 0px 0px;">
                                <div class="msg_text">
                                    <textarea id="msg"></textarea>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary recordDone" data-dismiss="modal">Close</button>
                <button id="send" type="button" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>
<script>
function getRecord(id) {
    $('#modal_ticket').val(id);
    $(".lBar").removeClass('hidden');
    $.post("{{url('/api/ticket/record/get')}}", {
            'ticketId': id,
            "_token": $('#token').val()
        },
        function(data) {
        	$(".lBar").addClass('hidden');
            var list = data['list'];
            var line = "<tr><td><div class='{0} msg'><div class='title'>{1}</div><div class='body'>{2}</div><div>{3}</div></div></td></tr>";
            var board = $('#recordContent');
            board.html('');
            list.forEach(function(item) {
                var tmp = line;
                tmp = tmp.replace('{0}', item['sender_type']);
                tmp = tmp.replace('{1}', item['sender_name']);
                tmp = tmp.replace('{2}', item['message']);
                tmp = tmp.replace('{3}', item['create_time']);
                board.append(tmp);
            });
            $('#recordModal').modal('show');
        });
}

$("#recordDone").click(function() {
    $('#modal_ticket').val('');
    $('#recordContent').html('');
});

$("#send").click(function() {
    var msg = $('#msg').val();
    if (msg == '') {
        return;
    }
    $(".lBar").removeClass('hidden');
    $.post("{{url('ticket/record/update')}}", {
            'ticket_id': $('#modal_ticket').val(),
            'message': msg,
            'userId': 1,
            'roleName': "ROOT",
        },
        function(data) {
            var success = data['err'];
            if (success == 0) {
                getRecord($('#modal_ticket').val());
            }
        });
});
</script>
@endsection