<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ app()->getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game Panel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/css/normalize.css">
	<link href="/css/app.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/themify-icons.css">
    <link rel="stylesheet" href="/css/flag-icon.min.css">
    <link rel="stylesheet" href="/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/lib/vector-map/jqvmap.min.css">
   	<link rel="stylesheet" href="/css/bootstrap-datepicker3.standalone.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.min.css" rel='stylesheet' type='text/css'>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <!--  <script src="/js/vendor/jquery-2.1.4.min.js"></script> -->
    <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.min.js"></script>
	<script src="/js/bootstrap-datepicker.min.js"></script>
	
	<style>
	   .pagination li {padding: 8px 16px;}
	   .pagination .current a {color: blue}
	   .hidden {display:none !important}
	   .lBar {position:fixed;top:0;left:0;width:100%;height:100%;background-color:rgba(0, 0, 0, 0.4);}
       .lBar .lHint { position:absolute; top:50%; left:50%; line-height:20px;margin-top:-10px; color: #FFF;}
	</style>
	<script src="/js/pagination.js"></script>
</head>
<body>
	<div id='app'></div>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel" style="min-width:200px">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="/img/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="/img/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                	<li><h3 class="menu-title">Platform</h3></li>
                	<li><a href="/account/search">Account Search</a></li>
                    <li><a href="/order/search">Order Search</a></li>
                	<li><h3 class="menu-title">Crystal Hearts</h3></li>
                	<li><a href="/event/master">Create Events</a></li>
                    <li><a href="/event/panel">Events Panel</a></li>

                    <li><h3 class="menu-title">Game</h3></li>
                    <li><a href="/announcement">Announcement</a></li>

                	<li><h3 class="menu-title">SDK</h3></li>
                    <li><a href="/game">Games</a></li>
                    <li><a href="/floatingBtn">Floating Button</a></li>
                    <li><a href="/productTest">Product Test</a></li>
                    <li><a href="/activityEvent">Activity Event</a></li>
                    <li><a href="/activityEventProduct">Activity Event Product</a></li>
                    <li><a href="/facebookActivity">Facebook Activity</a></li>
                    <li><a href="/exchangeRecords">Exchange Records</a></li>
                    <li><h3 class="menu-title">Order</h3></li>
                    <li><a href="/order/panel">Testing</a></li>
					<li><h3 class="menu-title">Administration</h3></li>
                    @if(Session::has('user') && Session::get('user')->username == 'root')
                        <li><a href="/user/createView">Create Account</a></li>
                    @endif
					<li><a href="/user/pwd">Change Password</a></li>
					<li><a href="/logout">Logout</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->
	  <!-- Right Panel -->

    <div id="right-panel" class="right-panel">
        <div class="content mt-3">
        	<div class="animated fadeIn">
        		<div class="row">
        			<div class="col-md-12">
        				@yield('content')
        			</div>
        		</div>
			</div>
        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->
    
    <div class="lBar hidden">
		<span class="lHint">Loading...</span>
	</div>
	@yield('modal')
    @yield('modal2')
    @yield('modal3')
    <script src="/js/main.js"></script>
</body>
</html>