@extends('template.main') @section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Create User</strong>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <form action="{{url('/user/create')}}" method="post" id="createForm" style="padding: 20px;">
        <div class="row">
            <div class="form-group col-md-4">
                <label for="Name">User Name</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="User Name" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="Password">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="ConfirmPassword">Confirm Password</label>
                <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-2">
                <label for="role">Role</label>
                <select name="role" id="role" class="form-control" required>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-primary rounded">Create</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
@if(session()->has('message'))
    alert(message);
@endif
$('#createForm').submit(function(ev) {
    if($('#password').val() != $('#confirm_password').val()) {
        ev.preventDefault();
        alert("Please check Password and ConfirmPassword are the same.");
        return;
    }
    this.submit();
});
</script>
@endsection