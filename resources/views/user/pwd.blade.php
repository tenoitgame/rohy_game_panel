@extends('template.main')

@section('content')
<div class="card">
    <div class="card-header">
        <strong class="card-title">Change Password</strong>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <form action="{{url('/user/changePassword')}}" method="post" id="passwordForm" style="padding: 20px;">
        <div class="row">
            <div class="form-group col-md-4">
                <label for="pwd_original">Original Password</label>
                <input type="password" class="form-control" name="pwd_original" id="pwd_original" placeholder="Original Password" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="pwd_new">New Password</label>
                <input type="password" class="form-control" name="pwd_new" id="pwd_new" placeholder="New Password" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="pwd_confirm">Confirm Password</label>
                <input type="password" class="form-control" name="pwd_confirm" id="pwd_confirm" placeholder="Confirm Password" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-primary rounded">Send</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
@if(session()->has('message'))
    alert(message);
@endif
$('#passwordForm').submit(function(ev) {
    if($('#pwd_new').val() != $('#pwd_confirm').val()) {
        ev.preventDefault();
        alert("Please check Password and ConfirmPassword are the same.");
        return;
    }
    this.submit();
});
</script>
@endsection