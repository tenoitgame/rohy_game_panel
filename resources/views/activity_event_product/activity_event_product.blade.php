@extends('template.main') 
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
    table { table-layout: fixed; }
    table th, table td { overflow: hidden; padding: 10px;}
</style>
@section('content')
<div class="card">
    @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="card-header">
        <strong class="card-title">Activity Event Product</strong>
        <div class="row">
            <a href="#addModal" class="btn btn-success rounded" data-toggle="modal">
                <span>Add Activity Event Product</span>
            </a>       
        </div>
    </div>
    <div class="card-body">

        <table id="activityEventProduct" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>ActivityEventId</th>
                    <th>ActivityEventType</th>
                    <th>ActivityEventGameName</th>
                    <th>GameItemId</th>
                    <th>Number</th>
                    <th>CreateTime</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var table = $('#activityEventProduct').DataTable( {
        lengthChange: false,
        ajax: "{{ '/api/activityEventProduct/get' }}",
        columns: [
            { data: "id", width: "5%"},
            { data: "ActivityEventId", width: "10%" },
            { data: "ActivityEventType", width: "10%" },
            { data: "ActivityEventGameName", width: "10%" },
            { data: "game_item_id", width: "10%" },
            { data: "num", width: "10%" },
            { data: "create_time", width: "10%" },
            { data: null, defaultContent:"<a href='#editModal' class='edit btn btn-primary rounded' data-toggle='modal'><span>Edit</span></a>","width": "10%"},
        ],
        select: true
    });
    table
        .order( [ 1, 'asc' ], [ 4, 'asc' ] )
        .draw();

    $('#activityEventProduct tbody').on( 'click', 'a.edit', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        $("#editModal [name='id']").val(data['id']);
        $("#editModal [name='ActivityEventId']").val(data['ActivityEventId']);
        $("#editModal [name='GameItemId']").val(data['game_item_id']);
        $("#editModal [name='Number']").val(data['num']);
    });
});
</script>
@endsection

@section('modal')
<div id="addModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/activityEventProduct/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Add Activity Event Product</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>ActivityEventId</label>
                        <select class="form-control" name="ActivityEventId">
                            @foreach($activityEvents as $row)
                            <option value='{{ $row['id'] }}'>{{ $row['id'] }} / {{ $row['type'] }} / {{ $row['game_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>GameItemId</label>
                        <input type="text" class="form-control" name="GameItemId" required>
                    </div>
                    <div class="form-group">
                        <label>Number</label>
                        <input type="text" class="form-control" name="Number" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('modal2')
<div id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/activityEventProduct/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Activity Event Product</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input id="id" name="id" required style="display: none;">
                    <div class="form-group">
                        <label>ActivityEventId</label>
                        <input type="text" class="form-control" name="ActivityEventId" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label>GameItemId</label>
                        <input type="text" class="form-control" name="GameItemId" required>
                    </div>
                    <div class="form-group">
                        <label>Number</label>
                        <input type="text" class="form-control" name="Number" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection