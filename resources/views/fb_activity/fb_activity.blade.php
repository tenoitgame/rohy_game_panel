@extends('template.main') 
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
    table { table-layout: fixed; }
    table th, table td { overflow: hidden; padding: 10px;}
</style>
@section('content')
<div class="card">
    @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="card-header">
        <strong class="card-title">Fb Activity</strong>
{{--         <div class="row">
            <a href="#addGameModal" class="btn btn-success rounded" data-toggle="modal">
                <span>Add Fb Activity</span>
            </a>       
        </div> --}}
    </div>
    <div class="card-body">

        <table id="fbActivity" class="table" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>GameName</th>
                    <th>ActivityEventId</th>
                    <th>Invite Limit</th>
                    <th>CreateTime</th>
                    <th>Action1</th>
                    <th>Action2</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var table = $('#fbActivity').DataTable( {
        lengthChange: false,
        ajax: "{{ '/api/facebookActivity/get' }}",
        columns: [
            { data: "id", width: "5%"},
            { data: "type", width: "10%", render : function (data, type, row) {
                return data == 1 ? 'Share' : 'Invitation';
            }},
            { data: "gameCode", width: "10%" },
            { data: "activity_event_id", width: "15%" },
            { data: "inviteLimit", width: "10%", render : function (data, type, row) {
                return row['type'] == 1 ? '-' : data;
            }},
            { data: "createTime", width: "10%" },
            { data: null, defaultContent:"<a href='#editModal' class='edit btn btn-primary rounded' data-toggle='modal'><span>Edit</span></a>","width": "7%"},
            { data: null, defaultContent:"<a href='#editDetailModal' class='detail btn btn-primary rounded' data-toggle='modal'><span>Detail</span></a>","width": "7%"},
        ],
        select: true
    });

    $('#fbActivity tbody').on( 'click', 'a.detail', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        $("#editDetailModal [name='id']").val(data['id']);
        $("#editDetailModal [name='Type']").val(data['type']);
        $.post( "{{url('api/facebookActivity/getDetail')}}", {
            'id': data['id'],
            'Type': data['type'],
            '_token': '{{ csrf_token() }}'
            },
            function( data ) {
                data.forEach(function(element) {
                    switch(element['language']) {
                        case 'en':
                            $("#editDetailModal [name='idEN']").val(element['id']);
                            $("#editDetailModal [name='packageNameEN']").val(element['packageName']);
                            break;
                        case 'zh':
                            $("#editDetailModal [name='idZH']").val(element['id']);
                            $("#editDetailModal [name='packageNameZH']").val(element['packageName']);
                            break;
                        case 'zh_tw':
                            $("#editDetailModal [name='idZHTW']").val(element['id']);
                            $("#editDetailModal [name='packageNameZHTW']").val(element['packageName']);
                            break;
                        case 'id':
                            $("#editDetailModal [name='idID']").val(element['id']);
                            $("#editDetailModal [name='packageNameID']").val(element['packageName']);
                            break;
                        case 'th':
                            $("#editDetailModal [name='idTH']").val(element['id']);
                            $("#editDetailModal [name='packageNameTH']").val(element['packageName']);
                            break;
                        case 'vi':
                            $("#editDetailModal [name='idVI']").val(element['id']);
                            $("#editDetailModal [name='packageNameVI']").val(element['packageName']);
                            break; 
                    }
                });
            });
    });

    $('#fbActivity tbody').on( 'click', 'a.edit', function () {
        var row_id = $(this).parents('tr');
        var data = table.row(row_id).data();

        $("#editModal [name='id']").val(data['id']);
        $("#editModal [name='Type']").val(data['type']).change();
        $("#editModal [name='GameCode']").val(data['gameCode']);
        $("#editModal [name='ActivityEventId']").val(data['activity_event_id']);
        $("#editModal [name='InviteLimit']").val(data['inviteLimit']);
    });

    $("#addGameModal [name='Type']").change(function(){
        if($(this).val() == '1') {
            $("#addGameModal [name='InviteLimit']").attr('readonly', true);
            $("#addGameModal [name='InviteLimit']").val('');
        } else {
            $("#addGameModal [name='InviteLimit']").attr('readonly', false);
        }
    });

    $("#editModal [name='Type']").change(function(){
        if($(this).val() == '1') {
            $("#editModal [name='InviteLimit']").attr('readonly', true);
            $("#editModal [name='InviteLimit']").val('');
        } else {
            $("#editModal [name='InviteLimit']").attr('readonly', false);
        }
    });

    $("#addGameModal [name='GameCode']").change(function(){
        $("#addGameModal [name='ActivityEventId']").html('');
        $.post( "{{url('api/activityEvent/getDataByGameName')}}", {
            'GameName': $(this).val(),
            '_token': '{{ csrf_token() }}'
            },
            function( data ) {
                data.forEach(function(element) {
                    "<td>{0}</td>";
                    str = "<option value='{0}'>{1} : {2}</option>";
                    $("#addGameModal [name='ActivityEventId']").append(str.format(element['id'],element['id'],element['type']));
                });
            });
    });

    $( "#create" ).submit(function( event ) {
        if($("#addGameModal [name='GameCode']").val()=='') {
            alert("請選擇GameCode");
            event.preventDefault();
        }
    });
});
</script>
@endsection

@section('modal')
<div id="addGameModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="create" action="{{url('/facebookActivity/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Add Facebook Activity</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" name="Type">
                            <option value='1'>SHARE</option>
                            <option value="2">INVITATION</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>GameCode</label>
                        <select class="form-control" name="GameCode">
                            <option value=''>請選擇</option>
                            <option value='trsm'>trsm</option>
                            <option value="ch">ch</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>ActivityEventId</label>
                        <select class="form-control" name="ActivityEventId" required>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>InviteLimit</label>
                        <input type="text" class="form-control" name="InviteLimit" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Add">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('modal2')
<div id="editModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit" action="{{url('/facebookActivity/save')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Facebook Activity</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input id="id" name="id" required style="display: none;">
                    <div class="form-group">
                        <label>Type</label>
                        <input type="text" class="form-control" id="Type" name="Type" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label>GameCode</label>
                        <input type="text" class="form-control" id="GameCode" name="GameCode" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label>ActivityEventId</label>
                        <input type="text" class="form-control" name="ActivityEventId" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label>InviteLimit</label>
                        <input type="text" class="form-control" id="InviteLimit" name="InviteLimit" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('modal3')
<div id="editDetailModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{url('/facebookActivity/saveDetail')}}" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Package Name</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input name="id" style="display: none;">
                    <input name="Type" style="display: none;">
                    <div class="form-group">
                        <label>EN</label>
                        <input name="idEN" style="display: none;">
                        <input type="text" class="form-control" name="packageNameEN" required>
                    </div>
                    <div class="form-group">
                        <label>ID</label>
                        <input name="idID" style="display: none;">
                        <input type="text" class="form-control" name="packageNameID" required>
                    </div>
                    <div class="form-group">
                        <label>TH</label>
                        <input name="idTH" style="display: none;">
                        <input type="text" class="form-control" name="packageNameTH" required>
                    </div>
                    <div class="form-group">
                        <label>VI</label>
                        <input name="idVI" style="display: none;">
                        <input type="text" class="form-control" name="packageNameVI" required>
                    </div>
                    <div class="form-group">
                        <label>ZHTW</label>
                        <input name="idZHTW" style="display: none;">
                        <input type="text" class="form-control" name="packageNameZHTW" required>
                    </div>
                    <div class="form-group">
                        <label>ZH</label>
                        <input name="idZH" style="display: none;">
                        <input type="text" class="form-control" name="packageNameZH" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default rounded" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-success rounded" value="Save">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection