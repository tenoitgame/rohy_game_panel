<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Game Panel System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		<link href="/css/app.css" rel="stylesheet"/>
        <style>
            body {background:#eee;}
            h3 {text-align: center; margin-top: 30px;}
            .login-form {width: 300px;margin: auto; margin-top: 15px;}
        </style>
    </head>
    <body>
    	<div id='app'></div>
    	<h3>Game Panel System</h3>
        <form class="login-form" method="POST" action="{{ action('LoginController@doLogin') }}">
 				<input type="text" id="username" name="username" class="form-control" placeholder="Username" value="{{ old('username') }}" required autofocus>
				@if ($errors->has('username'))
					<span class="help-block">
						<strong>{{ $errors->first('username') }}</strong>
					</span>
				@endif
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
				@if ($errors->has('password'))
					<span class="help-block">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
				@endif
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me" name="remember"> Remember Me
                    </label>
                </div>
                 {{ csrf_field() }}
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
		</form>
		<script src="/js/app.js"></script>
    </body>
</html>
