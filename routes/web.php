<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'LoginController@getLoginView'])->name('login');
Route::get('/login', ['uses' => 'LoginController@getLoginView'])->name('login');
Route::get('/logout', ['uses' => 'LoginController@doLogout'])->name('loginOut');
Route::post('doLogin', 'LoginController@doLogin');

Route::group( ['middleware' => 'guest' ], function() {
    Route::get('/home', ['uses' => 'UserController@getHome'])->name('home');

    //---UserController---
    Route::get('/user/pwd', ['uses' => 'UserController@getPwdView']);
    Route::get('/user/createView', ['uses' => 'UserController@getCreateView']);
    Route::post('/user/create', ['uses' => 'UserController@create']);
    Route::post('/user/changePassword', 'UserController@changePassword');


    //--EventsController---
    //1.View
    Route::get('/event/master', ['uses' => 'EventController@getEventMasterView']);
    Route::get('/event/panel', ['uses' => 'EventController@getEventPanelView']);
    Route::get('/event/detail/{key}', ['uses' => 'EventController@getEventDetailPanelView']);
    ///
    Route::get('{locale?}/event', 'EventController@getEventView');
	Route::get('{locale?}/event/{id}', 'EventController@getEventDetailView');
	Route::get('{locale?}/reward', 'EventController@getRewardView');
	Route::get('{locale?}/reward/{id}', 'EventController@getRewardDetailView');
	///
    //2.API
    Route::post('/event/create', ['uses' => 'EventController@create']);
    Route::post('/api/event/getAll', ['uses' => 'EventController@getAll']);
    Route::post('/api/event/getByIDandLang', ['uses' => 'EventController@getAllByIDandLang']);
    Route::post('/api/event/getByIDandLangs', ['uses' => 'EventController@getAllByIDandLangs']);
    Route::post('/api/event/eventlang', ['uses' => 'EventController@createEventLang']);


    //--OrderController---
    //1.View
    Route::get('/order/panel', ['uses' => 'OrderController@getOrderPanelView']);
    Route::get('/order/search', 'OrderController@getSearchView');
    Route::get('/order/add', 'OrderController@getAddView');
   
    ///
    //2.API
    Route::post('/api/order/change', ['uses' => 'OrderController@change']);
    Route::post('/api/order/search', ['uses' => 'OrderController@search']);
    Route::post('/api/order/searchData', 'OrderController@searchData');
    Route::post('/api/order/verifyRequest', 'OrderController@verifyRequest');
    Route::post('/api/order/record/collect', 'OrderController@getRecord');
    Route::post('/api/order/toTestOrder', 'OrderController@toTestOrder');


    //************GAME SECTION ***********//
    // 1. VIEW
    Route::get('/game', ['uses' => 'GameController@getGameView']);
    Route::post('/game/save', ['uses' => 'GameController@save']);

    // 2. API
    Route::get('/api/game/get', ['uses' => 'GameController@getGameData']);


    //************FLOATING BUTTON SECTION ***********//
    // 1. VIEW
    Route::get('/floatingBtn', ['uses' => 'FloatingBtnController@getFloatingBtnView']);
    Route::post('/floatingBtn/save', ['uses' => 'FloatingBtnController@save']);

    // 2. API
    Route::get('/api/floatingBtn/get', ['uses' => 'FloatingBtnController@getFloatingBtnData']);


    //************PLAYER ACCOUNT SECTION ***********//
    // 1. VIEW
    Route::get('/account/search', 'AccountController@getSearchView');
    Route::get('/account/ban', 'AccountController@getBanView');
    Route::get('/account/banlist', 'AccountController@getBanListView');
    
    // 2. API
    Route::post('api/account/check', 'AccountController@checkUser');
    Route::post('api/account/ban', 'AccountController@banUser');
    Route::post('api/account/search', 'AccountController@searchData');
    Route::post('api/account/ban/list', 'AccountController@banlistData');
    Route::post('api/account/ban/unlock', 'AccountController@unlockUser');


    //************IN GAME PRODUCT TEST SECTION ***********//
    // 1. VIEW
    Route::get('/productTest', ['uses' => 'ProductApiTestController@getProductTestView']);
    Route::post('/productTest/test', ['uses' => 'ProductApiTestController@testProduct']);

    // 2. API
    

    //************ACTIVITY EVENT SECTION ***********//
    // 1. VIEW
    Route::get('/activityEvent', ['uses' => 'ActivityEventController@getExchangeEventView']);
    Route::post('/activityEvent/save', ['uses' => 'ActivityEventController@save']);

    // 2. API
    Route::get('/api/activityEvent/get', ['uses' => 'ActivityEventController@getExchangeEventData']);
    Route::post('/api/activityEvent/getDataByGameName', ['uses' => 'ActivityEventController@getDataByGameName']);


    //************FACEBOOK ACTIVITY SECTION ***********//
    // 1. VIEW
    Route::get('/facebookActivity', ['uses' => 'FbActivityController@getFbActivityView']);
    Route::post('/facebookActivity/save', ['uses' => 'FbActivityController@save']);
    Route::post('/facebookActivity/saveDetail', ['uses' => 'FbActivityController@saveDetail']);

    // 2. API
    Route::get('/api/facebookActivity/get', ['uses' => 'FbActivityController@getFbActivityData']);
    Route::post('/api/facebookActivity/getDetail', ['uses' => 'FbActivityController@getFbActivityDetailData']);

    //************ACTIVITY EVENT PRODUCT SECTION ***********//
    // 1. VIEW
    Route::get('/activityEventProduct', ['uses' => 'ActivityEventProductController@getActivityEventProductView']);
    Route::post('/activityEventProduct/save', ['uses' => 'ActivityEventProductController@save']);

    // 2. API
    Route::get('/api/activityEventProduct/get', ['uses' => 'ActivityEventProductController@getActivityEventProductData']);

    //************EXCHANGE RECORDS SECTION ***********//
    // 1. VIEW
    Route::get('/exchangeRecords', ['uses' => 'ExchangeRecordsController@getExchangeRecordsView']);

    // 2. API
    Route::get('/api/exchangeRecords/get', ['uses' => 'ExchangeRecordsController@getExchangeRecordsData']);
    Route::get('/api/exchangeRecords/logs/get', ['uses' => 'ExchangeRecordsController@getExchangeLogs']);


    //************ANNOUNCEMENT SECTION ***********//
    // 1. VIEW
    Route::get('/announcement', ['uses' => 'AnnouncementController@getAnnouncementView']);
    Route::post('/announcement/save', ['uses' => 'AnnouncementController@save']);
    Route::post('/announcement/detail/save', ['uses' => 'AnnouncementController@saveDetail']);

    // 2. API
    Route::get('/api/announcement/get', ['uses' => 'AnnouncementController@getAnnouncementData']);
    Route::post('/api/announcement/detail/get', ['uses' => 'AnnouncementController@getAnnouncementDetailData']);
});
