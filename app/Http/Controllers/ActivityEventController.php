<?php
namespace App\Http\Controllers;
use App\Models\ActivityEvent;
use App\Models\FbLineActivity;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class ActivityEventController extends Controller {
    public function getExchangeEventView(Request $request){
        return view('activity_event.activity_event');
    }

    public function getExchangeEventData(Request $request){
        $activityEvents = ActivityEvent::all();
        return \DataTables::of($activityEvents)->make(true);
    }

    public function save(Request $request){
        $id = $request->get('id');
        $GameName = $request->get('GameName');
        $Type = $request->get('Type');
        $Description = $request->get('Description');

        if(empty($id)) {
            $activityEvent = new ActivityEvent;
        } else {
            $activityEvent = ActivityEvent::where('id', $id)->first();
        }
        
        $message = null;
        try {
            $activityEvent->game_name = $GameName;
            $activityEvent->type = $Type;
            $activityEvent->description = $Description;
            $activityEvent->save();
        } catch(QueryException $ex){ 
            $message = $ex->getMessage();
        }

        if($message == null && ($Type == "SHARE" || $Type == "INVITATION")) {
            $fbLineActivity = new FbLineActivity;
            $fbLineActivity->gameCode = $GameName;
            switch ($Type) {
                case "SHARE":
                    $fbLineActivity->type = 1;
                    $fbLineActivity->inviteLimit = 0;
                    break;
                case "INVITATION":
                    $fbLineActivity->type = 2;
                    $fbLineActivity->inviteLimit = 20;
                    break;
                default:
                    $fbLineActivity->type = 0;
                    break;
            }
            $fbLineActivity->activity_event_id = $activityEvent->id;
            $fbLineActivity->version = '1.0';
            $fbLineActivity->createTime = date("Y-m-d H:i:s");
            $fbLineActivity->save();
        }


        return redirect()->back()->with('message', $message);
    }

    public function getDataByGameName(Request $request){
        $GameName = $request->get('GameName');

        $activityEvents = ActivityEvent::where('game_name', $GameName)->get();
        return $activityEvents;
    }
}