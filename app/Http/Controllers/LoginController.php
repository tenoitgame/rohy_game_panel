<?php
namespace App\Http\Controllers;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    
    public function getLoginView(){
 
        if (session('user', null) == null) {
            return view('login');
        }
        return redirect()->route('home');
    }
    
    
    public function doLogin(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:255',
            'password' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('login')->withErrors($validator);
        }
        
        $username = $request->get('username');
        $password = $request->get('password');
        $remember = $request->get('remember');

        $user = (new Users)->findByUsername($username);
        // $users = DB::select('SELECT * FROM users');
        if ($user['password'] == md5($password)) {
            session(['user' => $user]);
            return redirect()->route('home');
        } else {
            $validator->errors()->add('login', 'Login failed!');
            return redirect()->route('login')->withErrors($validator);
        }
    }
    
    public function doLogout(Request $request) {
        $request->session()->flush();
        return redirect()->route('login');
    }
}