<?php
namespace App\Http\Controllers;
use App\Models\ActivityEventProduct;
use App\Models\ActivityEvent;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class ActivityEventProductController extends Controller {
    public function getActivityEventProductView(Request $request){
        $activityEvents = ActivityEvent::all();

        return view('activity_event_product.activity_event_product')->with('activityEvents', $activityEvents);
    }

    public function getActivityEventProductData(Request $request){
        $activityEventProducts = (new ActivityEventProduct)->getList();
        return \DataTables::of($activityEventProducts)->make(true);
    }

    public function save(Request $request){
        $id = $request->get('id');
        $ActivityEventId = $request->get('ActivityEventId');
        $GameItemId = $request->get('GameItemId');
        $Number = $request->get('Number');

        if(empty($id)) {
            $activityEventProduct = new ActivityEventProduct;
        } else {
            $activityEventProduct = ActivityEventProduct::where('id', $id)->first();
        }
        
        $message = null;
        try {
            $activityEventProduct->activity_event_id = $ActivityEventId;
            $activityEventProduct->game_item_id = $GameItemId;
            $activityEventProduct->num = $Number;
            $activityEventProduct->save();
        } catch(QueryException $ex){ 
            $message = $ex->getMessage();
        }


        return redirect()->back()->with('message', $message);
    }
}