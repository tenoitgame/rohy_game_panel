<?php
namespace App\Http\Controllers;
use App\Models\FbLineActivity;
use App\Models\FbLineActivityDetail;
use App\Models\FbLineActivityInvitDetail;
use App\Models\ActivityEvent;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class FbActivityController extends Controller {
    public function getFbActivityView(Request $request){
        $activityEvents = ActivityEvent::all();
        return view('fb_activity.fb_activity')->with('activityEvents', $activityEvents);
    }

    public function getFbActivityData(Request $request){
        $fbLineActivities = FbLineActivity::all();
        return \DataTables::of($fbLineActivities)->make(true);
    }

    public function getFbActivityDetailData(Request $request){
        $id = $request->get('id');
        $Type = $request->get('Type');

        if($Type == 1) {
            $model = FbLineActivityDetail::where('fbLineActivityId', $id)->get();
        } else {
            $model = FbLineActivityInvitDetail::where('fbLineActivityId', $id)->get();
        }

        return $model;
    }

    public function save(Request $request){
        $id = $request->get('id');
        $GameCode = $request->get('GameCode');
        $Type = $request->get('Type');
        $ActivityEventId = $request->get('ActivityEventId');
        $InviteLimit = $request->get('InviteLimit');

        if(empty($id)) {
            $fbLineActivity = new FbLineActivity;
        } else {
            $fbLineActivity = FbLineActivity::where('id', $id)->first();
        }
        
        $message = null;
        try {
            $fbLineActivity->gameCode = $GameCode;
            $fbLineActivity->type = $Type;
            $fbLineActivity->activity_event_id = $ActivityEventId;
            $fbLineActivity->inviteLimit = empty($InviteLimit) ? 0 : $InviteLimit;
            $fbLineActivity->version = '1.0';
            $fbLineActivity->createTime = date("Y-m-d H:i:s");
            $fbLineActivity->save();
        } catch(QueryException $ex){ 
            $message = $ex->getMessage();
        }


        return redirect()->back()->with('message', $message);
    }

    public function saveDetail(Request $request){
        $id = $request->get('id');
        $Type = $request->get('Type');

        $idEN = $request->get('idEN');
        $packageNameEN = $request->get('packageNameEN');
        $this->saveDetailByLocale($id, $Type, 'en', $idEN, $packageNameEN);

        $idID = $request->get('idID');
        $packageNameID = $request->get('packageNameID');
        $this->saveDetailByLocale($id, $Type, 'id', $idID, $packageNameID);

        $idZHTW = $request->get('idZHTW');
        $packageNameZHTW = $request->get('packageNameZHTW');
        $this->saveDetailByLocale($id, $Type, 'zh_tw', $idZHTW, $packageNameZHTW);

        $idTH = $request->get('idTH');
        $packageNameTH = $request->get('packageNameTH');
        $this->saveDetailByLocale($id, $Type, 'th', $idTH, $packageNameTH);

        $idVI = $request->get('idVI');
        $packageNameVI = $request->get('packageNameVI');
        $this->saveDetailByLocale($id, $Type, 'vi', $idVI, $packageNameVI);

        $idZH = $request->get('idZH');
        $packageNameZH = $request->get('packageNameZH');
        $this->saveDetailByLocale($id, $Type, 'zh', $idZH, $packageNameZH);

        return redirect()->back();
    }

    private function saveDetailByLocale($fbLineActivityId, $Type, $Locale, $id, $packageName) {
        if(empty($id)) {
            if($Type == 1) {
                $model = new FbLineActivityDetail;
            } else {
                $model = new FbLineActivityInvitDetail;
            }
        } else {
            if($Type == 1) {
                $model = FbLineActivityDetail::where('id', $id)->first();
            } else {
                $model = FbLineActivityInvitDetail::where('id', $id)->first();
            }
        }
        
        try {
            $model->packageName = $packageName;
            $model->fbLineActivityId = $fbLineActivityId;
            $model->status = 1;
            $model->language = $Locale;
            $model->save();
        } catch(QueryException $ex){ 
            $message = $ex->getMessage();
        }
    }
}