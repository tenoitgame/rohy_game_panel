<?php
namespace App\Http\Controllers;
use App\Models\Chevents;
use App\Models\Cheventsdetail;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
class EventController extends Controller
{
    
    public function getEventMasterView(Request $request){
        return view('event/master');
    }
    public function getEventPanelView(Request $request){
        return view('event/panel');
    }
    public function getEventDetailPanelView(Request $request,$key){
    	$lang="en";
    	$event = (new Cheventsdetail)->getDetailByEventID($lang,$key);
    	$langs = (new Language)->get();
        return view('event/eventdetail',['language'=>$langs,'event'=>$event,'key'=>$key]);
    }

    public function create(Request $request)
    {   
    	$chevent=$request->get('chevent');
    	
        $pieces = explode("-", $chevent);
        $start =  date("Y-m-d H:i", strtotime($pieces[0])); 
        $end =  date("Y-m-d H:i", strtotime($pieces[1])); 
        $name=$request->get('name');
        $in_game_event_id=$request->get('in_game_event_id');
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->route('event/panel')->withErrors($validator);
        }

        $event = new Chevents;
        $event->name = $name;
        $event->in_game_event_id = $in_game_event_id;
        $event->start = $start;
        $event->status = "Y";
        $event->end = $end;
        $event->save();

        $detail = new Cheventsdetail;
        $detail->name = $name;
        $detail->language = "en";
        $detail->status = "Y";
        $detail->banner_img = "none";
        $detail->menu_img = "none";
        $detail->event_text = "none";
        $detail->exchange_text = "none";
        $detail->id_ch_events = $event->id;
        $detail->save();
        if($event->id > 0) {
            return redirect()->back()->with('message', 'Success');
        }else{
            return redirect()->back()->with('message', 'Error');
        }   
     // echo"asdas"; 
    }
    
    public function getAll(Request $request){
        $object = (new Chevents)->getAllActive();
        // echo($object);
        return \DataTables::of($object)->make(true);
    }
    public function getAllByIDandLang(Request $request){
    	$evid=$request->get('evid');
    	$lid = $request->get('langid');
        $object = (new Cheventsdetail)->getAllByIDandLang($evid,$lid);
        // echo($object);
        return \DataTables::of($object)->make(true);
    }
    public function getAllByIDandLangs(Request $request){
    	$evid=$request->get('evid');
    	$lid = $request->get('langid');
        $object = (new Cheventsdetail)->getAllByIDandLang($evid,$lid);
        return $object;
    }
    
    public function createEventLang(Request $request){   
        $detail = new Cheventsdetail;
        $name = $request->get('eventlang');
        $lang = $request->get('langid');
        $banner = $request->get('bannerimglang');
        $menu = $request->get('menuimglang');
        $evid = $request->get('evid');
        $eventText = $request->get('eventText');
        $exchangeText = $request->get('exchangeText');
        $status = $request->get('status');
        $check=$detail->createEventLang($name,$lang,$banner,$menu,$evid,$status,$eventText,$exchangeText);
        if($check == "success") {
            return redirect()->back()->with('message', 'Success');
        }else{
            return redirect()->back()->with('message', 'Error');
        }

    }
    public function getEventView(Request $request){
    	
    	$lang="";
    	$event = (new Chevents)->getActiveEvents($lang);
    	// echo($event);
        return view('event/eventbg',["event"=>$event]);
    }
    public function getEventDetailView(Request $request,$lang,$id){
    	$event = (new Cheventsdetail)->getDetailByDetailEventID($lang,$id);
    	// var_dump()($event[0]);
    	// echo($event);
        return view('event/event_detail',["event"=>$event]);
    }
    public function getRewardView(Request $request,$lang){
    	// echo "213";
    	$event = (new Chevents)->getActiveEvents($lang);
    	// // echo($event[0]->banner_img);
    	// var_dump($event);
        return view('event/reward',["event"=>$event]);
    }
    public function getRewardDetailView(Request $request,$lang,$id){
    	$event = (new Cheventsdetail)->getDetailByEventID($lang,$id);
    	// echo($event[0]->banner_img);
        return view('event/reward_detail',["event"=>$event,"idreward"=>$id]);
    }
    
    
}