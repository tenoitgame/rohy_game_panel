<?php
namespace App\Http\Controllers;
use App\Models\Announcement;
use App\Models\AnnouncementDetail;
use Illuminate\Http\Request;

class AnnouncementController extends Controller {
    private $languages = array('en','indonesia','th','vi','zh-Hans');

    public function getAnnouncementView(Request $request){
        return view('announcement.announcement')->with('languages', $this->languages);
    }

    public function getAnnouncementData(Request $request){
        $announcements = Announcement::all();
        return \DataTables::of($announcements)->make(true);
    }

    public function getAnnouncementDetailData(Request $request){
        $announcementId = $request->get('announcementId');

        $announcementDetails = AnnouncementDetail::where('announcement_id', $announcementId)->get()->keyBy('language');
        return $announcementDetails;
    }

    public function save(Request $request){
        $id = $request->get('id');
        $type = $request->get('type');
        $description = $request->get('description');
        $startTime = $request->get('startTime');
        $active = $request->get('active');

        if(empty($id)) {
            $announcement = new Announcement;
        } else {
            $announcement = Announcement::where('id', $id)->first();
        }
        
        $announcement->type = $type;
        $announcement->inner_description = $description;
        $announcement->start_time = $startTime;
        $announcement->active = $active;
        $announcement->save();

        if(empty($id)) {
            foreach ($this->languages as $language) {
                $announcementDetail = new AnnouncementDetail;
                $announcementDetail->announcement_id = $announcement->id;
                if($language == 'indonesia') {
                    $language = 'id';
                }
                $announcementDetail->language = $language;
                $announcementDetail->save();
            }
        }

        return redirect()->back();
    }

    public function saveDetail(Request $request){
        $ids = $request->get('id');
        $titles = $request->get('title');
        $contents = $request->get('content');

        foreach ($ids as $key => $id) {
            $announcementDetail = AnnouncementDetail::where('id', $id)->first();
            $announcementDetail->title = $titles[$key];
            $announcementDetail->content = $contents[$key];
            $announcementDetail->save();
        }

        return redirect()->back();
    }
}