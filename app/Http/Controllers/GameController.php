<?php
namespace App\Http\Controllers;
use App\Models\Games;
use Illuminate\Http\Request;

class GameController extends Controller {
    public function getGameView(Request $request){
        return view('game.game');
    }

    public function getGameData(Request $request){
        $games = Games::all();
        return \DataTables::of($games)->make(true);
    }

    public function save(Request $request){
        $id = $request->get('id');
        $GameName = $request->get('GameName');
        $GameShortCode = $request->get('GameShortCode');
        $GameCode = $request->get('GameCode');
        $AndroidPackageName = $request->get('AndroidPackageName');
        $ItunesAppId = $request->get('ItunesAppId');
        $loginAllow = $request->get('loginAllow');

        if(empty($id)) {
            $game = new Games;
        } else {
            $game = Games::where('gid', $id)->first();
        }
        
        $game->gameName = $GameName;
        $game->gameShortCode = $GameShortCode;
        $game->gameCode = $GameCode;
        $game->package_name = $AndroidPackageName;
        $game->itunes_app_Id = $ItunesAppId;
        $game->loginAllow = $loginAllow;
        $game->save();

        return redirect()->back();
    }
}