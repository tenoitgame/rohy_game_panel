<?php
namespace App\Http\Controllers;
use App\Models\Users;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Maatwebsite\Excel\Excel;

class ProductApiTestController extends Controller {
    public function getProductTestView(Request $request){
        return view('product_api_test.product_api_test');
    }
    
    public function testProduct(Request $request){
        $gameName = $request->get('gameName');
        $UserId = $request->get('UserId');
        $Role = $request->get('Role');
        $Server = $request->get('Server');

        $file = $request->file('csvFile');
        $data = array();
        $time = time();

        $i = 1;
        foreach(file($file->path()) as $line) {
            $pOrderId = "Test".$time.$i;

            $products = explode(",", $line);

            //pOrderId + serverCode + roleId + userId + amount + time + key)
            $md5str = strtoupper(md5($pOrderId . $Server . $Role . $UserId . "5" . $time . "4AF32649AEC6B0F9EAC2F5BBDF72C9F0"));
            $client = new Client();
            $response = $client->request('POST', "http://113.196.75.190/GameServer/8888playEA/GameCoinRequest.php", [
                'form_params' => [
                    'gameCode'=>$gameName,
                    'userId'=>$UserId,
                    'roleId'=>$Role,
                    'pOrderId'=>$pOrderId,
                    'serverCode'=>$Server,
                    'currency'=>'USD',
                    'type'=>'ITEM',
                    'gameCoin'=>0,
                    'productId'=>str_replace(array("\r", "\n"), "", $products[0]),
                    'itemId'=>str_replace(array("\r", "\n"), "", $products[1]),
                    'md5Str'=>$md5str,
                    'amount'=>5,
                    'time'=>$time,
                    'orderStateMonth'=>'201806',
                    'bonus'=>0
                ]
            ]);

            $result = json_decode($response->getBody());
            $productResult['productId'] = $products[0];
            $productResult['itemId'] = $products[1];
            $productResult['result'] = $result;

            array_push($data, $productResult);

            $i += 1;
        }

        return redirect()->back()->with('data', $data);
    }
}