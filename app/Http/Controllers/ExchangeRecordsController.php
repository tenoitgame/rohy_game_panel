<?php
namespace App\Http\Controllers;
use App\Models\GameExchange;
use App\Models\GameExchangeLog;
use Illuminate\Http\Request;

class ExchangeRecordsController extends Controller
{
    public function getExchangeRecordsView(Request $request){
        return view('exchange_records/exchange_records');
    }

    public function getExchangeRecordsData(Request $request){
        $gameExchanges = GameExchange::all();
        return \DataTables::of($gameExchanges)->make(true);
    }

    public function getExchangeLogs(Request $request){
        $game_exchange_id = $request->get('game_exchange_id');
        $gameExchangeLogs = GameExchangeLog::where('game_exchange_id', $game_exchange_id);
        return \DataTables::of($gameExchangeLogs)->make(true);
    }
}