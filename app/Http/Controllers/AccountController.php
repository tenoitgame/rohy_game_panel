<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\UserIDInfo;
use App\Models\UsersGameBan;
use App\Models\Account;
use App\Util\Pagination;

class AccountController extends Controller
{
    public function getSearchView(Request $request) {
        return view('account/search');
    }

    public function getBanView(Request $request) {
        return view('account.ban');
    }
    
    public function getBanListView(Request $request) {
        return view('account.banlist');
    }
    
    public function checkUser(Request $request) {
        // UserID Model
        $accountType = $request['accountType'];
        $accountName = $request['accountName'];
        $userIDInfo = new UserIDInfo;
        $userIds = [];
        if($accountType == 'GOOGLE'){
            $userIds = $userIDInfo-> findUidByGoogleID($accountName);
        } else if($accountType == 'FACEBOOK'){
            $userIds = $userIDInfo-> findUidByFaceBookID($accountName);
        } else if($accountType == 'NORMAL'){
            $userIds = $userIDInfo-> findUidByUserName($accountName);
        } else if($accountType == 'UID'){
            $userIds = $userIDInfo-> findUidByUserId($accountName);
        }
        
        $count = count($userIds);
        
        $r['err'] = 0;
        if ($count == 0) {
            $r['err'] = 1;
            $r['msg'] = 'NOT FOUND!!!!';
        } else if ($count > 1) {
            $r['err'] = 2;
            $r['msg'] = 'DB ERROR!!!!';
            $r['data'] = $userIds;
        } else {
            $r['msg'] = 'FOUND';
            $r['data'] = $userIds[0];
        }
            
        return $r;
        
    }
    
    public function banUser(Request $request) {
        $banType = $request['banType'];
        $uid = $request['uid'];
  
        if ($banType == 'all') {
            $userIDInfo = new UserIDInfo;
            $userIDInfo->banAccount($uid);
        } else {
            $usersGameBan = new UsersGameBan;
            $usersGameBan->banUserFromGame($uid, $banType);
        }
        
        $r['err'] = 0;
        $r['msg'] = 'SUCCESS';
        return $r;
    }
    
    public function unlockUser(Request $request) {
        $banType = $request['banType'];
        $uid = $request['uid'];
        
        if ($banType == 'all') {
            $userIDInfo = new UserIDInfo;
            $userIDInfo->unlockAccount($uid);
        } else {
            $usersGameBan = new UsersGameBan;
            $usersGameBan->unlockUserFromGame($uid, $banType);
        }
        
        $r['err'] = 0;
        $r['msg'] = 'SUCCESS';
        return $r;
    }

    public function banlistData(Request $request) {
        
        $banType = $request->input('banType');
        $page = $request->input('index');
        
        $limit = 10;
        $index = (($page - 1) * $limit);
        
        $total = 0;
        if ($banType == 'all') {
            $r = (new Account)->getBanListData($index, $limit);
            $data['list'] = $r['list'];
            $total = $r['count'];
        } else {
            $r = (new UsersGameBan)->getBanUserByGame($banType, $index, $limit);
            $data['list'] = $r['list'];
            $total = $r['count'];
        }

        $pagination = (new Pagination)->init($limit, $total, $page);
        
        //$pagination
        $data['pagination'] = $pagination;
        
        return $data;
    }
    
    public function searchData(Request $request) {
		$userId = $request->input('userId');
		$accountName = $request->input('accountName');
	
		$accountStatus = $request->input('accountStatus');
	    
	    $account = new Account;
	    $r = $account->getData($accountStatus, $userId, $accountName);

        return \DataTables::of($r)->make(true);
    }
}