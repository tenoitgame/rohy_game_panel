<?php
namespace App\Http\Controllers;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {
    public function getPwdView(Request $request){
        return view('user.pwd')->with('user', session('user', null));
    }
    
    public function getCreateView(Request $request){
        return view('user.create');
    }

    public function getHome(Request $request){
        return view('welcome');
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:255',
            'password' => 'required|max:255',
            'role' => 'required',
        ]);
        
        $username = $request->get('username');
        $password = $request->get('password');
        $role = $request->get('role');

	    $validator->after(function ($validator) use ($username){
	        if ($this->isUsernameExist($username) == true) {
	            $validator->errors()->add('username', 'This username has existed!');
	        }
	    });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $User = new Users;
        $User->username = $username;
        $User->password = md5($password);
        $User->role_id = $role;
        $User->save();

        if($User->id > 0) {
        	return redirect()->back()->with('message', 'Success');
        }else{
        	return redirect()->back()->with('message', 'Error');
        }
    }

    public function changePassword(Request $request) {
        $pwd_original = $request->get('pwd_original');
        $pwd_new = $request->get('pwd_new');
        $pwd_confirm = $request->get('pwd_confirm');
        
        if ($pwd_original == null || $pwd_original === '') {
            return redirect()->back()->with('message', 'old password cannot be empty');
        }
        
        if ($pwd_new == null || $pwd_new === '') {
            return redirect()->back()->with('message', 'new password cannot be empty');
        }
        
        if ($pwd_confirm == null || $pwd_confirm === '') {
            return redirect()->back()->with('message', 'confirm password cannot be empty');
        }
        
        if ($pwd_confirm != $pwd_new) {
            return redirect()->back()->with('message', 'confirm and new password not match');
        }
        $UserDB = (new Users);
        $username = session('user')['username'];
        
        $user = $UserDB->findByUsername($username);
        if ($user['password'] != md5($pwd_original)) {
            return redirect()->back()->with('message', 'password incorrect');
        }
        
        $UserDB->updatePassword($username, md5($pwd_new));
        
        return redirect()->back()->with('message', 'success');
    }

    private function isUsernameExist($username) {
    	$User = new Users;
    	return count($User->findByUsername2($username)) > 0;
    }
}