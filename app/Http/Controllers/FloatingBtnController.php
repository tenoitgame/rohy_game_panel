<?php
namespace App\Http\Controllers;
use App\Models\FloatingBtn;
use Illuminate\Http\Request;

class FloatingBtnController extends Controller {
    public function getFloatingBtnView(Request $request){
        return view('floating_btn.floating_btn');
    }

    public function getFloatingBtnData(Request $request){
        $floatingBtns = FloatingBtn::all();
        return \DataTables::of($floatingBtns)->make(true);
    }

    public function save(Request $request){
        $id = $request->get('id');
        $GameCode = $request->get('GameCode');
        $Version = $request->get('Version');
        $Status = $request->get('Status');

        if(empty($id)) {
            $floatingBtn = new FloatingBtn;
        } else {
            $floatingBtn = FloatingBtn::where('id', $id)->first();
        }
        
        $floatingBtn->gameCode = $GameCode;
        $floatingBtn->version = $Version;
        $floatingBtn->status = $Status;
        $floatingBtn->save();

        return redirect()->back();
    }
}