<?php
namespace App\Http\Controllers;
use App\Models\Rohyorder;
use App\Models\UserIDInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;
use Session;
use App\Util\Pagination;
use Illuminate\Support\Facades\Log;
use Exception;


class OrderController extends Controller
{
    
    public function getOrderPanelView(Request $request){
        $users = DB::table('users_testing')->get();
        return view('order/order_panel',['users'=>$users]);
    }
  
    public function search(Request $request){
    	$account = $request->get('account');
        // $account=272;
    	$order = (new Rohyorder)->getOrderByUser($account);
        return $order;
    }
    public function change(Request $request){
        $idorder = $request->get('idorder');
        $user = session('user');
        $order = (new Rohyorder)->getOrderByID($idorder);
        $no=$order[0]->rohy_order_no;     
        $user=$user['id'];     
        $order = (new Rohyorder)->changeOrderByID($no,$user);
        return redirect('order/panel');
    }

    public function getSearchView(Request $request) {
        return view('order.search');
    }
    
    public function searchData(Request $request) {
        $numOfRow = 10;
        $page = $request->input('index');
        $index = (($page - 1) * $numOfRow);
        
        $startTime = $request->input('startTime');
        $endTime = $request->input('endTime');
        
        $orderStatus = $request->input('orderStatus');
        
        $gameName = $request->input('gameName');
        
        $accountName = $request->input('accountName');
        $accountType = $request->input('accountType');
        
        // UserID Model
        $userIDInfo = new UserIDInfo;
        $userIds = [];
        if($accountType == 'GOOGLE'){
            $userIds = $userIDInfo-> findByGoogleID($accountName);
        } else if($accountType == 'FACEBOOK'){
            $userIds = $userIDInfo-> findByFaceBookID($accountName);
        } else if($accountType
         == 'NORMAL'){
            $userIds = $userIDInfo-> findByUserName($accountName);
        }
        
        $rohyOrder = new RohyOrder;
        $r = $rohyOrder->getData($startTime,$endTime,$index,$numOfRow,$orderStatus,$gameName,$userIds, true);
        
        //$pagination
        $data['pagination'] = (new Pagination)->init($numOfRow, $r['count'], $page);
        $data['list'] = $r['list'];
        $data['sql'] = $r['sql'];
        
        return $data;
    }

}