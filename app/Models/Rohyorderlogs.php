<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rohyorderlogs extends Model
{
    protected $table = 'payment.rohy_order_logs';
    protected $guarded = ['id'];
    public $timestamps = false;
    
    
}