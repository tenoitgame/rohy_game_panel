<?php 
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Cheventsdetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ch_events_detail';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//     protected $hidden = ['password', 'remember_token'];

    public $timestamps = false;
    public function getDetailByEventID($lang,$key)
    {
        $obj = DB::table('ch_events_detail')
        ->where('id_ch_events', '=', $key)
        ->where('language', '=', $lang)
        ->get();
        return $obj;
    }
    public function getDetailByDetailEventID($lang,$key)
    {
        $obj = DB::table('ch_events_detail')
        ->where('id', '=', $key)
        ->get();
        return $obj;
    }

    public function getAllByIDandLang($id,$lid)
    {
        $obj = DB::table('ch_events_detail')
        ->where('id_ch_events', '=', $id)
        ->where('language', 'like', '%'.$lid.'%')
        ->get();
        return $obj;
    }
    public function createEventLang($name,$lang,$banner,$menu,$evid,$status,$eventText,$exchangeText)
    {
        $event = Cheventsdetail::firstOrNew(array('id_ch_events' => $evid,'language' => $lang));
        $event->id_ch_events = $evid;
        $event->language = $lang;
        $event->name = $name;
        $event->banner_img = $banner;
        $event->menu_img = $menu;
        $event->event_text = $eventText;
        $event->exchange_text = $exchangeText;
        $event->status = $status;
        $event->save();
        if($event->id>0){
            return "success";
        }else{
            return "error";
        }
    }


}
