<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AnnouncementType extends Model
{
    protected $connection = 'gamepanel';
    protected $table = 'announcement_type';
    public $timestamps = false;
}