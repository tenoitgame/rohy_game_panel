<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FloatingBtn extends Model
{
    protected $connection = '8888play';
    protected $table = 'btn_control';
    public $timestamps = false;
}
