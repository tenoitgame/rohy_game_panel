<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Account extends Model
{
    protected $connection = '8888play';
    protected $table = 't_users';
    public $timestamps = false;

    public function getCount($userId, $gameCode) {
        $count = Account::where('userId', $userId)
                ->where('game_name', $gameCode)
                ->count();

        return $count;
    }
    
    
    public function getBanListData($index, $limit) {
        $users = Account::select('t_users.*', 'f.facebook_id', 'g.google_id')
        ->leftJoin('t_users_facebook as f', 't_users.uid', '=', 'f.t_users_uid')
        ->leftJoin('t_users_google as g', 't_users.uid', '=', 'g.t_users_uid')
        ->where('flag', '=', 'N');
        
        //  Query
        $r['count'] = $users->count();
        
        $d = $users
        ->skip($index)
        ->take($limit)
        ->get();
        
        $r['list'] = $d;
        
        return $r;
    }
    
    public function getData($accountStatus, $userId, $accountName) {
        $users = Account::select('t_users.*');
        
        // Optional
        if($userId != ''){
            $users->where('uid', $userId);
        }

        if($accountName != ''){
            $users->where('userName', $accountName);
        }
        
        if($accountStatus != ''){
            $count = count($accountStatus);
            if ($count == 1) {
                $users->where('flag',$accountStatus[0]);
            } else if ($count > 1){
                $users->whereIn('flag',$accountStatus);
            }
        }

        return $users->get();
    }
}