<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserIDInfo extends Model
{
   
    public function banAccount($uid) {
        DB::table('8888play.t_users')
        ->where('uid', $uid)
        ->update(['flag' => 'N']);
    }
    
    public function unlockAccount($uid) {
        DB::table('8888play.t_users')
        ->where('uid', $uid)
        ->update(['flag' => 'Y']);
    }
    
    public function findUidByGoogleID($googleId) {
        $d = DB::table('8888play.t_users_google')
        ->select('t_users_uid')
        ->where('google_id', $googleId)
        ->get();
        return $d;
    }
    
    public function findByGoogleID($googleId) {
        
        $d = DB::table('8888play.t_users_google as t1')
        ->select('t2.userid' )
        ->leftJoin('8888play.t_users as t2', 't1.t_users_uid', '=', 't2.uid')
        ->where('t1.google_id', $googleId)
        ->get();
        return $d; 
    }

    public function findUidByFaceBookID($facebookId) {
        
        $d = $users = DB::table('8888play.t_users_facebook')
        ->select('t_users_uid')
        ->where('facebook_id', $facebookId)
        ->get();
        
        return $d;
    }
    
    public function findByFaceBookID($facebookId) {
        
        $d = $users = DB::table('8888play.t_users_facebook as t1')
        ->select('t2.userid' )
        ->leftJoin('8888play.t_users as t2', 't1.t_users_uid', '=', 't2.uid')
        ->where('t1.facebook_id', $facebookId)
        ->get();
        
        return $d; 
    }
  
    public function findUidByUserId($userId) {
        
        $d = $users = DB::table('8888play.t_users')
        ->where('uid','=', $userId)
        ->get();
        
        return $d;
    }
    
    public function findByUserId($userId) {
        
        $d = $users = DB::table('8888play.t_users')
        ->where('userid','=', $userId)
        ->get();
        
        return $d;
    }
    
    public function findUidByUserName($accountName) {
        
        $d = DB::table('8888play.t_users')
        ->select('uid' )
        ->where('userName',  '=' ,  $accountName)
        ->get();
        
        return $d;
    }
    
    public function findByUserName($accountName) {
        
        $d = DB::table('8888play.t_users')
        ->select('userid' )
        ->where('userName',  'like' ,  '%'.$accountName.'%')
        ->get();
        
        return $d; 
    }
}
