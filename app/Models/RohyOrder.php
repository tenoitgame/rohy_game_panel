<?php 
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class Rohyorder extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment.rohy_order';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//     protected $hidden = ['password', 'remember_token'];

    public $timestamps = false;

	public function getOrderByUser($id)
    {
        $order = Rohyorder::where('user_uid', $id)
                ->get();
        return $order;
    }
    public function getOrderByID($id)
    {
        $order = Rohyorder::where('uid', $id)
                ->get();
        return $order;
    }
    public function changeOrderByID($id,$user)
    {
        $change=Rohyorder::where('rohy_order_no', $id)
          ->update(['order_status' => 'TESTING']);

        $log = new Rohyorderlogs;

        $log->rohy_order_no = $id;
        $log->order_status = 'TESTING';
        $log->message = "{Action:'testing',User:'".$user."'}";

        $log->save();
    }

    public function getData($startTime,$endTime,$index,$limit,$orderStatus,$gameName,$userId, $isCount) {
        $endTime = date('Y-m-d', strtotime($endTime . ' +1 day'));
        DB::connection()->enableQueryLog();
        $sql = DB::table('payment.rohy_order as t1')
        // Must 
        ->where('t1.create_time', '>=', $startTime)
        ->where('t1.create_time', '<=', $endTime);
       
        
        // Optional

        if($orderStatus != ''){
            $count = count($orderStatus);
            if ($count == 1) {
                $sql->where('t1.order_status',$orderStatus[0]);
            } else if ($count > 1){
                $sql->whereIn('t1.order_status',$orderStatus);
            }
        }
        
        
        if($gameName != ''){
            $sql->where('t1.game_name',$gameName);
        }
        

        if(count($userId) != 0 ){
             $userIdArray  =[];
            foreach($userId as $key) {
                array_push($userIdArray, $key->userid);
            }
            $sql->whereIn('t1.userId',$userIdArray);
        }
        
        //  Count
        if ($isCount) {
            $r['count'] = $sql->count();
        }
        
        //  Query
        $list = $sql
        ->leftJoin('8888play.t_users as t2', 't1.user_uid', '=', 't2.uid')
        ->leftJoin('payment.product_info as t3', 't1.product_info_uid', '=', 't3.uid')
        ->skip($index)
        ->take($limit)
        ->orderBy('t1.create_time', 'desc')
        ->get();
        
        $r['list'] = $list;
        $r['sql'] = DB::getQueryLog();
        
        return $r;
    }
    
    public function getRatioPrice($id) {
        DB::connection()->enableQueryLog();
        $sql = DB::table('payment.rohy_order as t1') 
        ->select('t1.rohy_order_no', 't1.create_time', 't1.game_name',
        DB::raw('t2.ratio as finalprice')) 
        ->leftJoin('payment.game_coin_ratio as t2', function($join) 
            { 
                $join->on('t1.currency', '=', 't2.currency'); 
                $join->on('t1.game_name', '=', 't2.game_name'); 
            }) 
        ->where('t1.rohy_order_no','=',$id)
        ->first();
        $r['data'] = $sql;
        $r['sql'] = DB::getQueryLog();
        
        return $r['data'];
    }

}
