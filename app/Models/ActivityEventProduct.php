<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActivityEventProduct extends Model
{
    protected $connection = 'exchange';
    protected $table = 'activity_event_product';
    public $timestamps = false;

    public function getList() {
    	return ActivityEventProduct::select('activity_event_product.*', 'E.id as ActivityEventId', 'E.type as ActivityEventType', 'E.game_name as ActivityEventGameName')
        ->leftJoin('activity_event as E', 'activity_event_product.activity_event_id', '=', 'E.id')
        ->get();
    }
}
