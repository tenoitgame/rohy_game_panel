<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FbLineActivity extends Model
{
    protected $connection = '8888play';
    protected $table = 'fb_line_activity';
    public $timestamps = false;
}
