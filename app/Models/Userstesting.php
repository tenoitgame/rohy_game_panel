<?php 
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Users extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_testing';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//     protected $hidden = ['password', 'remember_token'];

    public $timestamps = false;

	public function findByUsername($username)
	{
 	    Users::setConnection('mysql'); 	    
 	    return Users::where('username', $username)
 	    ->leftJoin('role', 'users.role_id', '=', 'role.id')
 	    ->select('users.*', 'role.role_name')
 	    ->first();
	}


}
