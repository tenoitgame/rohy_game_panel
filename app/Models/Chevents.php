<?php 
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class Chevents extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ch_events';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//     protected $hidden = ['password', 'remember_token'];

    public $timestamps = false;

    public function getAllActive()
    {
        $event = Chevents::where('status', 'Y')
                ->get();
        return $event;
    }
    public function getActiveEvents($lang)
    {
        $mytime = Carbon::now();
        // $now=$mytime->toDateTimeString();
        $event = Chevents::select('ch_events.*', 'd.*')
        ->leftJoin('ch_events_detail as d', 'ch_events.id', '=', 'd.id_ch_events')
        ->where('ch_events.status', 'Y')
        ->where('language','like', '%'.$lang.'%')
        ->where('ch_events.end','>',$mytime)
        ->where('ch_events.start','<',$mytime)
        ->get();
        return $event;
    }
    
    

}
