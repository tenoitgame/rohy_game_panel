<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActivityEvent extends Model
{
    protected $connection = 'exchange';
    protected $table = 'activity_event';
    public $timestamps = false;
}