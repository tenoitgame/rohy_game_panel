<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GameExchangeLog extends Model
{
    protected $connection = 'exchange';
    protected $table = 'game_exchange_log';
}
