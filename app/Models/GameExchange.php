<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GameExchange extends Model
{
    protected $connection = 'exchange';
    protected $table = 'game_exchange';
    public $timestamps = false;
}
