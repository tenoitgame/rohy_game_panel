<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AnnouncementDetail extends Model
{
    protected $table = 'announcement_detail';
    public $timestamps = false;
}