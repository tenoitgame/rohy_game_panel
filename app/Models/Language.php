<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Language extends Model
{
    protected $table = 'cscpanel.language';
    protected $guarded = ['id'];
    public $timestamps = false;
    
    
}