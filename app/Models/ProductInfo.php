<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Account extends Model
{
    protected $connection = 'payment';
    protected $table = 'product_info';
    public $timestamps = false;
}