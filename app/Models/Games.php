<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Games extends Model
{
    protected $connection = '8888play';
    protected $table = 't_games';
    public $timestamps = false;
    protected $primaryKey = 'gid';
}
