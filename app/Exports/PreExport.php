<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\VirtualRegister;

class PreExport implements FromCollection
{
    private $game = '';
    
    public function setGame($game) {
        $this->game = $game;
    }
    
    public function collection()
    {
        $r = (new VirtualRegister)->getRecordByGameName($this->game);
        $data = collect();
        $data->push(['Email', 'Pre-Register Code']);
        
        foreach ($r as $row) {
            $data->push([$row->email, $row->code]);
        }
        
        return $data;
    }
}