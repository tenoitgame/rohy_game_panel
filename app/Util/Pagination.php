<?php
namespace App\Util;

class Pagination
{

    private $obj_page;

//     function __construct()
//     {
//         $page['firstPage'] = 1; //首頁
//         $page['fromPage'] = 1; //大於5頁後的第一頁
//         $page['toPage'] = 1; //大於5頁後的最後一頁
//         $page['idxPage'] = 1; //目前頁數
//         $page['pageRows'] = 10; //一次幾筆
//         $page['total'] = 0; //總共筆數
//         $page['total_page'] = 1; //總共筆數頁數
//         $obj_page = $page;
//     }
    
    function init($num_row, $total_item, $target_page)
    {
    	if ($total_item == null) {
    		$total_item = 0;
    	}
    	
    	//強制無條件進位
    	$this->obj_page['total'] = $total_item;
    	$this->obj_page['total_page'] = ceil($total_item / $num_row); //總頁數
        $this->obj_page['pageRows'] = 10;
    	//預設頁
    	if ($target_page > $this->obj_page['total_page']){
    		$this->obj_page['idxPage'] = (int)$this->obj_page['total_page'];
    	} else {
    		$this->obj_page['idxPage'] = (int)$target_page;
    	}
    	
    	
    	//一個subTab為十頁,演算第十頁不進位
    	$this->obj_page['fromPage'] = floor($this->obj_page['idxPage'] /  $this->obj_page['pageRows']) *  $this->obj_page['pageRows']; 
    	
    	
    	//如果總頁數小於一次顯示頁數
    	
    	if ($this->obj_page['total_page'] < $this->obj_page['pageRows']){
    		$this->obj_page['toPage'] = $this->obj_page['total_page'] - 1; //最後現是頁為最後一頁

    	} else {
    		$boundary = $this->obj_page['fromPage'] + $this->obj_page['pageRows'] - 1;
    		if ($boundary < $this->obj_page['total_page']){
    			$this->obj_page['toPage'] = $boundary;
    		} else {
    			$this->obj_page['toPage'] = $this->obj_page['total_page'] - 1; //最後現是頁為上頁
    		}
    	}	
    	
    	
    	$this->obj_page['fromPage'] += 1; //右移一位
    	$this->obj_page['toPage'] += 1; //右移一位
    	
    	if ($this->obj_page['fromPage'] >= $this->obj_page['pageRows']){

    		$this->obj_page['firstPage'] = 1; //增加前一頁
    	} else {
    		$this->obj_page['firstPage'] = 0; //不加前一頁
    	}
    	
    	
    	if ($this->obj_page['toPage'] < $this->obj_page['total_page']){
    		$this->obj_page['lastPage'] = 1; //增加下一頁
    	} else {
    		$this->obj_page['lastPage'] = 0; //不加下一頁
    	}
    	return $this->obj_page;
    }
}
