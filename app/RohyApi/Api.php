<?php

namespace App\RohyApi;

use Config;
use Session;
use GuzzleHttp\Client;

class Api
{
    private static $GetTokenVerifyKey = "h0j9k8l7!@#$";

    public static function post($url, $parameters) {
        // $token = self::getPaymentToken();
        // if(empty($token)) {
        //     return null;
        // }

        $client = new Client();
        $res = $client->request('POST', $url, [
            'json' => $parameters,
            'headers' => [
                'content-type' => 'application/json',
                'Accept' => 'application/json',
                // 'UserToken' => $token,
            ],
        ]);
        $response = json_decode((string)$res->getBody());

        return $response;
    }
}
