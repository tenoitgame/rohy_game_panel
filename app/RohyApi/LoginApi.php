<?php

namespace App\RohyApi;

use Config;
use GuzzleHttp\Client;

class LoginApi extends Api
{
    public static function verifyRequest($rohyOrderNo) {
        $url = Config::get('rohy_api.loginUrl').'/rohy_sdklogin/game/verifyRequest';
        $parameter = [
                'orderNo' => $rohyOrderNo,
            ];

        return self::post($url, $parameter);
    }
}
